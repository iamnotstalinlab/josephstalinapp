﻿// Объект предложения
//


using System;

namespace JosephStalin.DataModels {
    public class FullOfferInfo : BaseOfferInfo {

        public string PickUpAddress { get; set; }
        public string PayMentMethod { get; set; }

        /// Конкретные документы (медицинская книжка, водительские права и тд)
        public bool SpecificDocuments { get; set; }

        ///Конкретный внешний вид, если важно как именно будет выглядить
        ///помощник на точке (Ростовая кукла, костюм, плакат)
        public bool SpecificView { get; set; }

        //Если важно гражданство РФ (паспорт или любые док-ты заменяющие его).
        public bool Documents { get; set; }

        //Пояснения от заказчика по предложению
        //(Здесь контактную информацию о заказчике будем оставлять на текущем этапе)
        public string Comment { get; set; }

        //Имя заказчика
        public string CustomerName { get; set; }

        //Телефон заказчика (на текущем этапе можно пропустить)
        public string CustomerContact { get; set; }

        //Имя помощнника
        public string PromoterName { get; set; }

        //Телефон помощника
        public string PromoterPhone { get; set; }

        //Статус приглашения (принято, пришло, ожидает и тд)
        //(Сейчас нас не интересует, оставляем пустым)
        public string Status { get; set; }

        //Радиус до места работы по прямой
        public double Radius { get; set; }

        //Время создания приглашения
        public DateTime Timestamp { get; set; }

        public FullOfferInfo() { }

        public FullOfferInfo(BaseOfferInfo info) {
            Date = info.Date;
            FromTime = info.FromTime;
            ToTime = info.ToTime;
            Wage = info.Wage;

            FullTimeMins = info.FullTimeMins;
            PickUpLat = info.PickUpLat;
            PickUpLng = info.PickUpLng;
            Id = info.Id;
        }
    }


    public class BaseOfferInfo {

        //Дата приглашения (когда выходить на точку)
        public string Date { get; set; }


        //Время начала работы на точке
        public string FromTime { get; set; }

        //Время окончания работы на точке
        public string ToTime { get; set; }


        //Оплата за всю работу
        public int Wage { get; set; }

        //Полные временные затраты на работу
        //(Считает приложение клиента)
        public double FullTimeMins { get; set; }


        //Координаты места помощи (точка рекламы, точка продаж и тд)
        //Широта
        public double PickUpLat { get; set; }

        //Долгота
        public double PickUpLng { get; set; }

        //Уникальный номер приглашения
        public int Id { get; set; }
    }




}
