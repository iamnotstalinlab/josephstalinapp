﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Views;
using Android.Gms.Maps.Model;
using Google.Places;
using Android.Content;
using Android.Support.Design.Widget;
using Android.Icu.Util;
using Com.Wdullaer.Materialdatetimepicker.Time;
using System;
using Android.Support.V4.View;
using JosephStalin.Adapters;
using System.Collections.Generic;
using Google.Android.Material.Navigation;
using Mapsui.UI;
using JosephStalin.Views;

namespace JosephStalin {
    [Activity(Label = "@string/app_name", Theme = "@style/JosephStalinTheme", MainLauncher = false, ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]

    public class MainActivity : AppCompatActivity
        , Com.Wdullaer.Materialdatetimepicker.Time.TimePickerDialog.IOnTimeSetListener {

        #region CREATE ACTIVITY AND CONNECT VIEWS
        /// Создаю макет
        ///

        protected override void OnCreate(Bundle savedInstanceState) {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);
            ConnectViews();
        }


        /// Связываю представления с макетом
        ///

        //Views
        Android.Support.V4.Widget.DrawerLayout drawerLayout;
        Android.Support.V7.Widget.Toolbar mainToolbar;
        NavigationView navView;

        //Fragments
        MainFragments.MapsFragment mapsFragment = new MainFragments.MapsFragment();
        MainFragments.HelpFragment helpFragment = new MainFragments.HelpFragment();

        StaticViewPager mainPager;

        void ConnectViews() {
            //Layouts
            drawerLayout = (Android.Support.V4.Widget.DrawerLayout)FindViewById(Resource.Id.drawerLayout);

            navView = (NavigationView)FindViewById(Resource.Id.navView);
            mainPager = (StaticViewPager)FindViewById(Resource.Id.mainPager);

            navView.NavigationItemSelected += NavView_NavigationItemSelected;

            //Fragments
            mapsFragment.SetRequestTime += MapFragment_SetRequestTime;

            helpFragment.OpenSalary += HelpFragment_OpenSalary;
            helpFragment.OpenCommunity += HelpFragment_OpenCommunity;

            //Toolbar
            mainToolbar = (Android.Support.V7.Widget.Toolbar)FindViewById(Resource.Id.mainToolbar);
            SetSupportActionBar(mainToolbar);
            SupportActionBar.Title = "";

            Android.Support.V7.App.ActionBar actionBar = SupportActionBar;
            actionBar.SetHomeAsUpIndicator(Resource.Mipmap.ic_menu_action);
            actionBar.SetDisplayHomeAsUpEnabled(true);

            InitializationData();
        }

        /// Устанавливаю и заполняю начальными данными страничный адаптер
        ///

        //Adapter
        MainAdapter mainAdapter;
        

        void InitializationData() {
            mainAdapter = new MainAdapter(SupportFragmentManager);
            mainAdapter.AddFragment(mapsFragment);
            mainAdapter.AddFragment(helpFragment);

            mainPager.Adapter = mainAdapter;
            mainPager.IsLockedSwipe = true;
        }



        #endregion

        #region MAP FRAGMENT EVENTS
        /// Вызываю диалог установки времени 
        /// 

        private void MapFragment_SetRequestTime(object sender, MainFragments.MapsFragment.RequestTimeEventArgs e) {
            
            string title;

            //Определяю какое именно время устанавиливает пользователь
            switch (e.FromOrTo) {
                case 'f':
                    title = "Время начала:";
                    break;
                case 't':
                    title = "Время окончания:";
                    break;
                default:
                    title = string.Empty;
                    break;
            }
            SetTime(ref title);
        }

        #endregion

        #region HELP FRAGMENT EVENTS
        /// Открываю страницу сообщества
        /// 

        private void HelpFragment_OpenCommunity(object sender, EventArgs e) {
            string uriString = "https://t.me/JosephStalinCommunity";
            OpenExternalResource(ref uriString);
        }

        /// Открываю страницу с жалованием
        /// 

        private void HelpFragment_OpenSalary(object sender, EventArgs e) {
            string uriString = "https://iamnotstalin.ru/titry/";
            OpenExternalResource(ref uriString);
        }

        #endregion

        #region MAIN ACTIVITY EVENTS

        /// Обрабатываю нажатие на элемент бокового меню (слева)
        /// 

        public override bool OnOptionsItemSelected(IMenuItem item) {
            switch (item.ItemId) {

                case Android.Resource.Id.Home:
                    drawerLayout.OpenDrawer((int)GravityFlags.Left);
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }

        /// Обрабатываю переход между основными страницами фрагементов прилжения ("Карты" и "Помощь")
        /// 

        private void NavView_NavigationItemSelected(object sender, NavigationView.NavigationItemSelectedEventArgs e) {
            string id = e.MenuItem.ToString();


            switch (e.MenuItem.ToString()) {
                case "Карта":
                    mainPager.SetCurrentItem(0, false);
                    drawerLayout.CloseDrawers();
                    break;
                case "Помощь":
                    mainPager.SetCurrentItem(1, false);
                    drawerLayout.CloseDrawers();
                    break;
            }
        }


        #endregion

        #region SET TIME

        /// Обрабатыва выбор времени из диалога
        /// 

        void SetTime(ref string title) {

            Calendar now = Calendar.Instance;
            Com.Wdullaer.Materialdatetimepicker.Time.TimePickerDialog timePickerDialog
                = Com.Wdullaer.Materialdatetimepicker.Time.TimePickerDialog.NewInstance(
                    this
                    , now.Get(CalendarField.Hour)
                    , now.Get(CalendarField.Minute)
                    , 1
                    , true
                    );

            timePickerDialog.Title = title;
            timePickerDialog.Show(FragmentManager, title);
        }

        /// Передаю данные из диалога выбора времени в фрагмент карт
        /// 

        public void OnTimeSet(RadialPickerLayout p0, int hourOfday, int minutes, int p) {

            string time = $"{hourOfday}:{string.Format("{0:00}", minutes)}";
            mapsFragment.TimeSet(ref time);
        }

        #endregion

        #region COMMON
        /// Открываю внешние ресурсы приложения и сылки
        /// 

        void OpenExternalResource(ref string uriString) {
            Android.Net.Uri exIntentUri = Android.Net.Uri.Parse(uriString);
            Intent exIntent = new Intent(Intent.ActionView, exIntentUri);
            StartActivity(exIntent);
        }

        /// Обрабатываю паузу приложения (свёрнуто/прикрыто и тд)
        ///

        protected override void OnPause() {
            base.OnPause();

            //При паузе выключаю приложение, потому что фрагмент карт
            //почему-то теряет данные. Долго искал из-за чего и не нашёл.
            //Разбираться буду, походу ещё 40 лет, поэтому просто вырубаю приложение.

            //Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
        }


        #endregion

    }
}