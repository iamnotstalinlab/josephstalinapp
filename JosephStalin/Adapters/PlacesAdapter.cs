﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Android.Graphics;
using AndroidX.RecyclerView.Widget;
using Android.Views;
using Android.Widget;
using System.Net;
using Android.Gms.Maps.Model;

namespace JosephStalin.Adapter {
    public class PlacesAdapter : RecyclerView.Adapter {

        List<DisplayPlace> dPlaces;
        public class DisplayPlace {
            public string DisplayName { get; set; }
            public string IconURL { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }

            public DisplayPlace(string name, string iconURL) {
                DisplayName = name;
                IconURL = iconURL;
            }
        }

        //Создаю адаптер и наполняю его первоначальными данными
        //

        public PlacesAdapter(ref List<DisplayPlace> places) {
            dPlaces = places;
        }

        // Обновляю все данные
        //

        public void UpdateData(ref List<DisplayPlace> places) {
            dPlaces = places;
        }

        //Создаю карточку предложения
        //

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.PlaceItem, parent, false);

            var vh = new PlacesAdapterViewHolder(itemView, OnItemClick);
            return vh;
        }

        public override int ItemCount => dPlaces.Count();


        //BindViewHolder
        public override void OnBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

            var holder = viewHolder as PlacesAdapterViewHolder;
            if (dPlaces != null) {

                RefreshHolderData(ref holder, position);
            }

        }


        //Событие при выборе элемента нажатием
        public event EventHandler<PlacesAdapterClickEventArgs> ItemClick;

        public class PlacesAdapterClickEventArgs : EventArgs {
            public View View { get; set; }
            public Int32 Position { get; set; }
        }

        void OnItemClick(PlacesAdapterClickEventArgs arg) => ItemClick?.Invoke(this, arg);



        public class PlacesAdapterViewHolder : RecyclerView.ViewHolder {

            public TextView nameText { get; set; }
            public LinearLayout placeLayout { get; set; }

            public ImageView iconImage { get; set; }

            //// Создаю карточку предложения и соединяю все представления
            ////

            public PlacesAdapterViewHolder(View itemView,
                Action<PlacesAdapterClickEventArgs> itemClickListener) : base(itemView) {

                nameText = (TextView)itemView.FindViewById(Resource.Id.nameText);
                iconImage = (ImageView)itemView.FindViewById(Resource.Id.iconImage);

                //Выбор элемента
                placeLayout = (LinearLayout)itemView.FindViewById(Resource.Id.placeLayout);

                placeLayout.Click += (sender, e) => itemClickListener(new PlacesAdapterClickEventArgs{
                    View = itemView,
                    Position = AdapterPosition
                });




            }
        }



        // Обновляю данные в карточке предложения
        //

        private void RefreshHolderData(ref PlacesAdapterViewHolder holder, int position) {
            if (position < dPlaces.Count) {
                holder.nameText.Text = dPlaces[position].DisplayName;

                if (!string.IsNullOrEmpty(dPlaces[position].IconURL)) {
                    holder.iconImage.SetImageBitmap(GetImageBitmapFromUrl(dPlaces[position].IconURL));
                }
            }
        }

        private Bitmap GetImageBitmapFromUrl(string url) {
            
            Bitmap imageBitmap = null;

            using (var webClient = new WebClient()) {
                var imageBytes = webClient.DownloadData(url);
                if (imageBytes != null && imageBytes.Length > 0) {
                    imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                }
            }
            return imageBitmap;
        }




        private void AllRefreshHoldersData(ref PlacesAdapterViewHolder holder) {
            for(int i =0; i <dPlaces.Count; ++i) {
                holder.nameText.Text = dPlaces[i].DisplayName;
            }
        }

    }
}
