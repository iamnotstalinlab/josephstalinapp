﻿using System;
using System.Collections.Generic;
using Android.Support.V4.App;
using static Android.Support.V4.View.ViewPager;

namespace JosephStalin.Adapters {
    public class MainAdapter : FragmentPagerAdapter, IOnPageChangeListener{

        private List<Android.Support.V4.App.Fragment> fragments = new List<Android.Support.V4.App.Fragment>();

        public override int Count => fragments.Count;

        public override Android.Support.V4.App.Fragment GetItem(int position) {
            return fragments[position];
        }

        public void AddFragment(Android.Support.V4.App.Fragment fragment) {
            fragments.Add(fragment);
        }

        public void OnPageScrollStateChanged(int state) {
            //throw new NotImplementedException();
        }

        public void OnPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            //throw new NotImplementedException();
        }

        public void OnPageSelected(int position) {
            //throw new NotImplementedException();
        }

        

        public MainAdapter(Android.Support.V4.App.FragmentManager fragmentManager) :base(fragmentManager) {

        }
    }
}