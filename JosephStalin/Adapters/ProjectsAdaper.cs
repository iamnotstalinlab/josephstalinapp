﻿
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//using Android.App;
//using Android.Content;
//using Android.Graphics;
//using Android.OS;
//using Android.Runtime;
//using Android.Support.V7.Widget;
//using Android.Util;
//using Android.Views;
//using Android.Widget;
//using JosephStalin.DataModels;

//namespace JosephStalin.Adapters {
//    public class ProjectsAdapter : RecyclerView.Adapter {

//        public event EventHandler<MarketAdapterClickEventArgs> ItemButtonClick;

//        List<BaseInfo> Items;
//        List<InvitationAdapterViewHolder> holders = new List<InvitationAdapterViewHolder>();

//        //Colors
//        Color mainColor;
//        Color secondColor;


//        public ProjectsAdapter(ref List<BaseInfo> Data) {

//            Items = Data;
//        }

//        public void UpdateData(ref List<BaseInfo> Data) {

//            Items = Data;
//            if (holders.Count > Items.Count) {
//                BaseInfo temp;

//                for (int i = 0; i < holders.Count; ++i) {

//                    temp = Items.Find(item => item.InvitationId == holders[i].Id);
//                    if (temp == null) {
//                        holders.RemoveAt(i);
//                    }
//                }
//            }
//        }

//        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
//            View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.project, parent, false);

//            var vh = new InvitationAdapterViewHolder(itemView, OnNextStepClick);

//            mainColor = itemView.Resources.GetColor(Resource.Color.jsMain);
//            secondColor = itemView.Resources.GetColor(Resource.Color.jsSecond);

//            holders.Add(vh);
//            return vh;
//        }

//        public override int ItemCount => Items.Count();



//        //BindViewHolder
//        public override void OnBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

//            //var holder = viewHolder as MarketAdapterViewHolder;
//            if (holders.Count > position) {

//                RefreshHolderData(position);

//                string status = Items[position].Status;
//                switch (status) {
//                    case "Жду":
//                        DimMode(position);
//                        break;
//                    case "Выполняюсь":
//                        BrightMode(position);
//                        break;
//                }
//            }
//        }

//        void OnNextStepClick(MarketAdapterClickEventArgs arg) => ItemButtonClick?.Invoke(this, arg);

//        public class InvitationAdapterViewHolder : RecyclerView.ViewHolder {

//            public string Id;

//            public TextView statusText { get; set; }
//            public TextView statusWageText { get; set; }
//            public TextView moneyText { get; set; }


//            public TextView dateText { get; set; }
//            public TextView wageText { get; set; }
//            public TextView fromTimeText { get; set; }
//            public TextView toTimeText { get; set; }

//            public LinearLayout statusLayout { get; set; }

//            public ImageButton actionButton { get; set; }

//            public InvitationAdapterViewHolder(View itemView,
//                Action<MarketAdapterClickEventArgs> buttonClickListener) : base(itemView) {

//                //Layouts
//                statusLayout = (LinearLayout)itemView.FindViewById(Resource.Id.statusLayout);

//                //TextView
//                statusText = (TextView)itemView.FindViewById(Resource.Id.statusText);
//                dateText = (TextView)itemView.FindViewById(Resource.Id.dateText);
//                wageText = (TextView)itemView.FindViewById(Resource.Id.wageText);
//                fromTimeText = (TextView)itemView.FindViewById(Resource.Id.fromTimeText);
//                toTimeText = (TextView)itemView.FindViewById(Resource.Id.toTimeText);
//                moneyText = (TextView)itemView.FindViewById(Resource.Id.moneyText);

//                //Buttons
//                actionButton = (ImageButton)itemView.FindViewById(Resource.Id.actionButton);

//                actionButton.Click += (sender, e) => buttonClickListener(new MarketAdapterClickEventArgs {
//                    View = itemView,
//                    Position = AdapterPosition
//                });
//            }
//        }

//        public class MarketAdapterClickEventArgs : EventArgs {
//            public View View { get; set; }
//            public Int32 Position { get; set; }
//        }

//        #region SetViewMode
//        public void BrightMode(int position) {


//            holders[position].dateText.SetTextColor(secondColor);
//            holders[position].dateText.SetTypeface(Typeface.Default, TypefaceStyle.Normal);

//            holders[position].fromTimeText.SetTextColor(mainColor);
//            holders[position].fromTimeText.SetTypeface(Typeface.Default, TypefaceStyle.Bold);

//            holders[position].toTimeText.SetTextColor(mainColor);
//            holders[position].toTimeText.SetTypeface(Typeface.Default, TypefaceStyle.Bold);

//            holders[position].wageText.SetTextColor(mainColor);
//            holders[position].wageText.SetTypeface(Typeface.Default, TypefaceStyle.Bold);

//            holders[position].statusText.SetTextColor(mainColor);
//        }

//        private void DimMode(int position) {

//            holders[position].dateText.SetTextColor(mainColor);
//            holders[position].dateText.SetTypeface(Typeface.Default, TypefaceStyle.Bold);

//            holders[position].fromTimeText.SetTextColor(secondColor);
//            holders[position].fromTimeText.SetTypeface(Typeface.Default, TypefaceStyle.Normal);

//            holders[position].toTimeText.SetTextColor(secondColor);
//            holders[position].toTimeText.SetTypeface(Typeface.Default, TypefaceStyle.Normal);

//            holders[position].wageText.SetTextColor(secondColor);
//            holders[position].wageText.SetTypeface(Typeface.Default, TypefaceStyle.Normal);

//            holders[position].statusText.SetTextColor(secondColor);
//        }

//        #endregion

//        private void RefreshHolderData(int position) {
//            holders[position].Id = Items[position].Id;

//            holders[position].dateText.Text = Items[position].Date;
//            holders[position].wageText.Text = Items[position].Wage.ToString();
//            holders[position].fromTimeText.Text = Items[position].FromTime;
//            holders[position].toTimeText.Text = Items[position].ToTime;
//            holders[position].statusText.Text = Items[position].Status;
//        }

//        public void RefreshAllData() {

//            if (holders.Count > 0) {
//                for (int i = 0; i < holders.Count; i++) {
//                    RefreshHolderData(i);
//                }
//            }
//        }


//    }
//}
