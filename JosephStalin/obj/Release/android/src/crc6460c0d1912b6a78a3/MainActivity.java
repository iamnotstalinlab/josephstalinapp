package crc6460c0d1912b6a78a3;


public class MainActivity
	extends androidx.appcompat.app.AppCompatActivity
	implements
		mono.android.IGCUserPeer,
		com.wdullaer.materialdatetimepicker.time.TimePickerDialog.OnTimeSetListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"n_onOptionsItemSelected:(Landroid/view/MenuItem;)Z:GetOnOptionsItemSelected_Landroid_view_MenuItem_Handler\n" +
			"n_onPause:()V:GetOnPauseHandler\n" +
			"n_onTimeSet:(Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;III)V:GetOnTimeSet_Lcom_wdullaer_materialdatetimepicker_time_RadialPickerLayout_IIIHandler:Com.Wdullaer.Materialdatetimepicker.Time.TimePickerDialog/IOnTimeSetListenerInvoker, Xamarin.Bindings.MaterialDateTimePicker\n" +
			"";
		mono.android.Runtime.register ("JosephStalin.MainActivity, JosephStalin", MainActivity.class, __md_methods);
	}


	public MainActivity ()
	{
		super ();
		if (getClass () == MainActivity.class)
			mono.android.TypeManager.Activate ("JosephStalin.MainActivity, JosephStalin", "", this, new java.lang.Object[] {  });
	}


	public MainActivity (int p0)
	{
		super (p0);
		if (getClass () == MainActivity.class)
			mono.android.TypeManager.Activate ("JosephStalin.MainActivity, JosephStalin", "System.Int32, mscorlib", this, new java.lang.Object[] { p0 });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);


	public boolean onOptionsItemSelected (android.view.MenuItem p0)
	{
		return n_onOptionsItemSelected (p0);
	}

	private native boolean n_onOptionsItemSelected (android.view.MenuItem p0);


	public void onPause ()
	{
		n_onPause ();
	}

	private native void n_onPause ();


	public void onTimeSet (com.wdullaer.materialdatetimepicker.time.RadialPickerLayout p0, int p1, int p2, int p3)
	{
		n_onTimeSet (p0, p1, p2, p3);
	}

	private native void n_onTimeSet (com.wdullaer.materialdatetimepicker.time.RadialPickerLayout p0, int p1, int p2, int p3);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
