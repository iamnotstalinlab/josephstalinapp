package crc6456b123d5c3898913;


public class PlacesAdapter_PlacesAdapterViewHolder
	extends androidx.recyclerview.widget.RecyclerView.ViewHolder
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("JosephStalin.Adapter.PlacesAdapter+PlacesAdapterViewHolder, JosephStalin", PlacesAdapter_PlacesAdapterViewHolder.class, __md_methods);
	}


	public PlacesAdapter_PlacesAdapterViewHolder (android.view.View p0)
	{
		super (p0);
		if (getClass () == PlacesAdapter_PlacesAdapterViewHolder.class)
			mono.android.TypeManager.Activate ("JosephStalin.Adapter.PlacesAdapter+PlacesAdapterViewHolder, JosephStalin", "Android.Views.View, Mono.Android", this, new java.lang.Object[] { p0 });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
