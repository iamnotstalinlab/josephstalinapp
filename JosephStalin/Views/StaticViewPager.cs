﻿using System;
using Android.Support.V4.View;
using Android.Views;
using Android.Content;

namespace JosephStalin.Views {


    public class StaticViewPager : ViewPager {

        public bool IsLockedSwipe { get; set; }

        Context mContext;

        public StaticViewPager(Context context): base(context) {}

        public StaticViewPager(Context context, Android.Util.IAttributeSet attrs) :base (context, attrs) {
            Init(context, attrs);
        }

        private void Init(Context context, Android.Util.IAttributeSet attrs) {
            mContext = context;
        }


        public override bool OnTouchEvent(MotionEvent e) {
            return !IsLockedSwipe && base.OnTouchEvent(e);
        }

        public override bool OnInterceptTouchEvent(MotionEvent ev) {
            return !IsLockedSwipe && base.OnInterceptTouchEvent(ev);
        }

        public override bool CanScrollHorizontally(int direction) {
            return !IsLockedSwipe && base.CanScrollHorizontally(direction);
        }
    }
}
