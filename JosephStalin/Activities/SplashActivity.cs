﻿//Экран обеспечивает отображение картинки пока загружается приложение
//

using System.Threading;
using Android.App;
using Android.OS;
using Android.Support.V7.App;
using JosephStalin.Helpers;

namespace JosephStalin.Activities {
    [Activity(Label = "@string/app_name", Theme ="@style/MyTheme.Splash", MainLauncher =true, NoHistory =true, ScreenOrientation =Android.Content.PM.ScreenOrientation.Portrait)]

    public class SplashActivity : AppCompatActivity {
        protected override void OnCreate(Bundle savedInstanceState) {
            base.OnCreate(savedInstanceState);

            // Create your application here
            Thread.Sleep(4000);
        }

        protected override void OnResume() {
            base.OnResume();
            StartActivity(typeof(LoginActivity));
        }


    }
}
