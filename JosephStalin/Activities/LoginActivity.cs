﻿// Модуль обеспечивает вход в систему и проверку участия
//

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Widget;
using JosephStalin;
using JosephStalin.EventListeners;
using JosephStalin.Helpers;
using JosephStalin.DataModels;
using Newtonsoft.Json;
using Android.Views;
using Google.Android.Material.Snackbar;

namespace JosephStalin.Activities {
    [Activity(Label = "PinCodeActivity", Theme = "@style/JosephStalinTheme",NoHistory =true,  ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class LoginActivity : Activity {

        bool isPhone =true;
        string number =string.Empty;
        string phone ="+7";

        //LinearLayout
        LinearLayout rootView;
        LinearLayout pinLayout;
        LinearLayout phoneLayout;

        //Buttons
        Button oneButton;
        Button twoButton;
        Button threeButton;
        Button fourButton;
        Button fiveButton;
        Button sixButton;
        Button sevenButton;
        Button eightButton;
        Button nineButton;
        Button zeroButton;
        ImageButton deleteButton;
        ImageButton acceptButton;

        //TextViews
        TextView firstNum;
        TextView secondNum;
        TextView thirdNum;
        TextView fourthNum;
        TextView helpText;

        TextView numberField;

        //Создаю модуль
        //
        Session session; //Текущая сессия, все основные настройки для данного запуска системы

        protected override void OnCreate(Bundle savedInstanceState) {
            base.OnCreate(savedInstanceState);

            // Create your application here

            session =new Session();
            SetContentView(Resource.Layout.login);
            ConnectViews();
        }

        protected void ConnectViews() {
            ConnectMainViews();
            ConnectPadViews();  
        }


        //Соединяю основные представления с объектами и провожу основную настройку
        //

        private void ConnectMainViews() {
            rootView = (LinearLayout)FindViewById(Resource.Id.rootView);
            pinLayout =(LinearLayout)FindViewById(Resource.Id.pinLayout);
            phoneLayout = (LinearLayout)FindViewById(Resource.Id.phoneLayout);

            helpText = (TextView)FindViewById(Resource.Id.helpText);

            if(!string.IsNullOrEmpty(session.GetPhone())) {

                isPhone =false;
                phone =session.GetPhone();

                phoneLayout.Visibility =ViewStates.Gone;
                pinLayout.Visibility =ViewStates.Visible;

                helpText.Text ="Введите пин-код для входа.";
            } else {
                isPhone =true;
                helpText.Text = "Введите действительный номер телефона.";
            }
        }

        //Соединяю представления табло с объектами
        //

        private void ConnectPadViews() {

            //Connect button pad
            oneButton = (Button)FindViewById(Resource.Id.oneButton);
            twoButton = (Button)FindViewById(Resource.Id.twoButton);
            threeButton = (Button)FindViewById(Resource.Id.threeButton);
            fourButton = (Button)FindViewById(Resource.Id.fourButton);
            fiveButton = (Button)FindViewById(Resource.Id.fiveButton);
            sixButton = (Button)FindViewById(Resource.Id.sixButton);
            sevenButton = (Button)FindViewById(Resource.Id.sevenButton);
            eightButton = (Button)FindViewById(Resource.Id.eightButton);
            nineButton = (Button)FindViewById(Resource.Id.nineButton);
            zeroButton = (Button)FindViewById(Resource.Id.zeroButton);
            deleteButton = (ImageButton)FindViewById(Resource.Id.deleteButton);
            acceptButton = (ImageButton)FindViewById(Resource.Id.acceptButton);

            oneButton.Click += (o, e) => {
                NumberFieldAdd('1');
            };
            twoButton.Click += (o, e) => {
                NumberFieldAdd('2');
            };
            threeButton.Click += (o, e) => {
                NumberFieldAdd('3');
            };
            fourButton.Click += (o, e) => {
                NumberFieldAdd('4');
            };
            fiveButton.Click += (o, e) => {
                NumberFieldAdd('5');
            };
            sixButton.Click += (o, e) => {
                NumberFieldAdd('6');
            };
            sevenButton.Click += (o, e) => {
                NumberFieldAdd('7');
            };
            eightButton.Click += (o, e) => {
                NumberFieldAdd('8');
            };
            nineButton.Click += (o, e) => {
                NumberFieldAdd('9');
            };
            zeroButton.Click += (o, e) => {
                NumberFieldAdd('0');
            };

            deleteButton.Click += (o, e) => {
                NumberFieldDelete();
            };

            acceptButton.Click += (o, e) => {
                isPhone =false;
                phoneLayout.Visibility =ViewStates.Gone;
                pinLayout.Visibility =ViewStates.Visible;
                helpText.Text ="Введите дважды одинаковый пин-код.";
            };

            //TextViews
            firstNum = (TextView)FindViewById(Resource.Id.firstNum);
            secondNum = (TextView)FindViewById(Resource.Id.secondNum);
            thirdNum = (TextView)FindViewById(Resource.Id.thirdNum);
            fourthNum = (TextView)FindViewById(Resource.Id.fourthNum);
            


            numberField = (TextView)FindViewById(Resource.Id.numberField);
            numberField.AfterTextChanged += NumberField_AfterTextChanged; ;
        }

        //При изменении содержимого в числовой панели проверяю полностью ли введён номер
        //

        private void NumberField_AfterTextChanged(object sender, Android.Text.AfterTextChangedEventArgs e) {
            if(phone.Length == 12) {
                acceptButton.Visibility = ViewStates.Visible;
            } else {
                acceptButton.Visibility = ViewStates.Gone;
            }
        }

        // Добавляю цифру в общее числовое поле
        //

        private void NumberFieldAdd(char inNum) {
            if (isPhone) {
                AddPhoneNum(inNum);
            } else {
                AddPinNum(inNum);
            }
        }

        // Добавляю цифру в телефонный номер
        //

        private void AddPhoneNum(char input) {
            if(phone.Length < 12) {

                phone += input;
                int length = phone.Length;
                if(length == 3 || length == 6 || length ==9 || length == 11) {

                    numberField.Text += " " + input;
                    return;
                }
                numberField.Text += input;
            }
        }

        // Добавляю цифру в пин-код
        //

        private void AddPinNum(char input) {
            if(number.Length < 4) {

                number += input;
                string blueColor = "#ff33b5e5";
                switch (number.Length) {
                    case 1:
                        firstNum.SetTextColor(Color.ParseColor(blueColor));
                        break;
                    case 2:
                        secondNum.SetTextColor(Color.ParseColor(blueColor));
                        break;
                    case 3:
                        thirdNum.SetTextColor(Color.ParseColor(blueColor));
                        break;
                    case 4:
                        fourthNum.SetTextColor(Color.ParseColor(blueColor));
                        CheckPinCode();
                        break;
                }
            }
        }


        // Удаляю цифру из общего числового поля
        //

        private void NumberFieldDelete() {
            if (isPhone) {
                PhoneNumDelete();
            } else {
                PinNumDelete();
            }
        }

        //Удаляю цифру из телефонного номера
        //

        private void PhoneNumDelete() {
            if(phone.Length > 2) {

                phone =phone.Remove(phone.Length - 1);
                int lenth = phone.Length;
                if(lenth ==2 || lenth ==5 || lenth ==8 || lenth == 10) {

                    numberField.Text = numberField.Text.Remove(numberField.Text.Length - 2);
                    return;
                }
                numberField.Text = numberField.Text.Remove(numberField.Text.Length -1);  
            }
        }

        // Удаляю цифру из пин-кода
        //

        private void PinNumDelete() {
            if(number.Length > 0) {

                string lightGreyColor = "#eeeeee";
                switch (number.Length) {
                    case 4:
                        fourthNum.SetTextColor(Color.ParseColor(lightGreyColor));
                        break;
                    case 3:
                        thirdNum.SetTextColor(Color.ParseColor(lightGreyColor));
                        break;
                    case 2:
                        secondNum.SetTextColor(Color.ParseColor(lightGreyColor));
                        break;
                    case 1:
                        firstNum.SetTextColor(Color.ParseColor(lightGreyColor));
                        break;
                }
                number =number.Remove(number.Length -1);
            }
        }


        //Проверяю ввод пин-кода

        int attempts =0;   //Попытки входа
        string pin; //Строка пин-кода

        private void CheckPinCode() {

            pin = session.GetPin();
            if (string.IsNullOrEmpty(pin)) {
                session.SavePin(number);
                ClearPad();
                helpText.Text ="Повторите пин-код.";
            } else {
                if(pin == number) {
                    LogInToServer(pin);
                } else {

                    //Считаю количестов неверных попыток входа, если слишком много, направляю за помощью
                    if(attempts < 8) {  

                        Snackbar.Make(rootView, "Вы ввели неверный пин-код.", Snackbar.LengthShort).Show();
                        attempts++;
                    } else {
                        LogInFailed(); // Направляю за помощью
                    }
                    ClearPad();
                }
            }

        }

        // Отправляю запрос к серверу на вход в систему
        //

        private void LogInToServer(string inPin) {
            TcpConnector connector = new TcpConnector(session.ServerIP, session.ServerPort);

            connector.Connect();
            Request request = new Request(phone, "Помощник", "Войти", 0, inPin);
            string json = JsonConvert.SerializeObject(request, Formatting.Indented);
            connector.SendData(json);

            json = connector.GetData();
            connector.SendData("Принял");
            connector.Close();

            request = JsonConvert.DeserializeObject<Request>(json);

            // Если сервер допустил до системы
            if(request.Status == "УСПЕХ") {
                session.SavePin(inPin);
                session.SavePhone(phone);
                StartActivity(typeof(MainActivity));
            } else {

                //Если сервер не допустил до системы
                ClearPad();
                LogInFailed();  //Направляю за помощью
            }
        }

        // Направляю пользователя за помощью
        //

        private void LogInFailed() {
            Android.Support.V7.App.AlertDialog.Builder acceptInvitationAlert = new Android.Support.V7.App.AlertDialog.Builder(this);
            acceptInvitationAlert.SetTitle("Ошибка входа.");

            string message = "Не получилось войти в систему. Предлагаю обратиться за помощью в общий чат.";

            acceptInvitationAlert.SetMessage(message);
            acceptInvitationAlert.SetPositiveButton("Открыть чат.", (senderAlert, args) => {
                OpenCommunityChat();
            });
            acceptInvitationAlert.SetNegativeButton("В другой раз.", (senderAlert, args) => {
                acceptInvitationAlert.Dispose();
            });

            acceptInvitationAlert.Show();
        }

        //Открываю общий чат сообщества
        //

        private void OpenCommunityChat() {
            string uriString = "https://t.me/JosephStalinCommunity";
            OpenExternalResource(ref uriString);
        }


        /// Открываю внешние ресурсы в других приложениях
        ///

        void OpenExternalResource(ref string uriString) {
            Android.Net.Uri exIntentUri = Android.Net.Uri.Parse(uriString);
            Intent exIntent = new Intent(Intent.ActionView, exIntentUri);
            StartActivity(exIntent);
        }


        // Очищаю табло для пин-кода и его данные
        //

        private void ClearPad() {

            string lightGreyColor = "#eeeeee";
            firstNum.Text = "*";
            secondNum.Text = "*";
            thirdNum.Text = "*";
            fourthNum.Text = "*";
            fourthNum.SetTextColor(Color.ParseColor(lightGreyColor));
            thirdNum.SetTextColor(Color.ParseColor(lightGreyColor));
            secondNum.SetTextColor(Color.ParseColor(lightGreyColor));
            firstNum.SetTextColor(Color.ParseColor(lightGreyColor));
            number = string.Empty;
        }
    }
}
