﻿
/// Фрагмент создаёт заявку и отправляет её на сервер
///

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Timers;
using Android;
using Android.App;
using Android.Content.PM;
using Android.Gms.Maps.Model;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using AndroidX.RecyclerView.Widget;
using Google.Android.Material.BottomSheet;
using Google.Android.Material.TextField;
using JosephStalin.Adapter;
using JosephStalin.DataModels;
using JosephStalin.EventListeners;
using JosephStalin.Helpers;
using Newtonsoft.Json;
using Refractored.Controls;
using Xamarin.Essentials;
using Mapsui.Utilities;
using Mapsui.UI.Android;
using static JosephStalin.Adapter.PlacesAdapter;
using Point = Mapsui.Geometries.Point;
using Mapsui.Projection;

namespace JosephStalin.MainFragments {
    public class MapsFragment : Android.Support.V4.App.Fragment {

        #region CREATE FRAGMENT AND CONNECT VIEWS
        /// Создаю фрагмент и рабочий сеанс (сессию)
        /// 

        Session session;
        public override void OnCreate(Bundle savedInstanceState) {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
            session = new Session();
            session.Open();

        }


        /// Создаю макет и связываю его с фрагментом
        ///
        View view;
        Activity activity;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            activity = this.Activity;

            // Use this to return your custom view for this Fragment
            view = inflater.Inflate(Resource.Layout.include_main, container, false);

            ConnectControl();
            CreateMap();
            return view;
        }

        /// Связываю представления с макетом
        ///

        //Layouts
        RelativeLayout layoutMyLocation;
        RelativeLayout layoutPickUpBig;
        RelativeLayout topInfoLayout;

        LinearLayout layoutAdditionData;
        LinearLayout layoutSheet;
        LinearLayout fullLayoutInfo;

        //TextViews
        TextView toTimeText;
        TextView fromTimeText;
        TextView dateText;
        TextView statusText;
        TextView statusWageText;
        TextView moneyText;

        TextView pickUpLocationBigText;
        TextView promoterNameText;
        TextView promoterNameFullText;

        //CheckBoxes
        CheckBox specificDocumentsCheck;
        CheckBox specificExteriorCheck;
        CheckBox documentsCheck;

        //ImageViews
        CircleImageView promoterImage;
        ImageView documentsIcon;
        ImageView specViewIcon;
        ImageView specDocumentsIcon;
        ImageView photoIcon;
        ImageView centerMarker;

        //EditText
        EditText promoWageText;

        //TextInputLayout
        TextInputLayout commentText;

        //Buttons
        Button locationSetButton;
        Button requestPromoButton;

        //BottomSheet
        BottomSheetBehavior invitationBottomSheetBehavior;
        FrameLayout inviteDetailsView;

        void ConnectControl() {

            //TextView
            statusText = (TextView)view.FindViewById(Resource.Id.statusText);
            statusWageText = (TextView)view.FindViewById(Resource.Id.statusWageText);
            toTimeText = (TextView)view.FindViewById(Resource.Id.toTimeText);
            fromTimeText = (TextView)view.FindViewById(Resource.Id.fromTimeText);
            dateText = (TextView)view.FindViewById(Resource.Id.dateText);
            pickUpLocationBigText = (TextView)view.FindViewById(Resource.Id.pickUpLocationBigText);
            moneyText = (TextView)view.FindViewById(Resource.Id.moneyText);

            toTimeText.Click += ToTimeText_Click;
            fromTimeText.Click += FromTimeText_Click;

            dateText.Click += DateText_Click;

            //EditText
            promoWageText = (EditText)view.FindViewById(Resource.Id.promoWageText);
            commentText = (TextInputLayout)view.FindViewById(Resource.Id.commentText);
            promoWageText.Clickable = true;
            promoWageText.AfterTextChanged += PromoFareText_AfterTextChanged;

            //ImageViews
            centerMarker = (ImageView)view.FindViewById(Resource.Id.centerMarker);

            //Buttons
            locationSetButton = (Button)view.FindViewById(Resource.Id.locationSetButton);
            requestPromoButton = (Button)view.FindViewById(Resource.Id.requestPromoButton);

            locationSetButton.Click += LocationSetButton_Click;
            requestPromoButton.Click += RequestPromoButton_Click;

            //Layouts
            layoutMyLocation = (RelativeLayout)view.FindViewById(Resource.Id.myLocation);
            layoutMyLocation.Click += MyLocationButton_Click;

            layoutAdditionData = (LinearLayout)view.FindViewById(Resource.Id.layoutAdditionData);
            layoutSheet = (LinearLayout)view.FindViewById(Resource.Id.layoutSheet);

            //BottomSheet
            inviteDetailsView = (FrameLayout)view.FindViewById(Resource.Id.inviteDetais_bottomsheet);
            invitationBottomSheetBehavior = BottomSheetBehavior.From(inviteDetailsView);

            //CheckBoxes
            documentsCheck = (CheckBox)view.FindViewById(Resource.Id.documentsCheck);
            specificDocumentsCheck = (CheckBox)view.FindViewById(Resource.Id.specificDocumentsCheck);
            specificExteriorCheck = (CheckBox)view.FindViewById(Resource.Id.specificExteriorCheck);

            ConnectPlacesRecycler(ref view);

            //SetDate
            dateText.Text = "Сегодня";
        }

        #endregion

        #region DISPLAY PLACES SEARCH
        //Отображаю поиск мест по запросу и вывожу похожие места списком.




        // Связываю представления из макета и объекты из кода.
        //

        //RecyclerView
        RecyclerView pRecycler;
        PlacesAdapter pAdapter;

        //EditText
        EditText pickUpLocationEdit;

        //TextView
        TextView pickUpText;

        //ProgressBar
        ProgressBar pProgress;

        //ImageView
        ImageView pIcon;

        List<DisplayPlace> dPlaces = new List<DisplayPlace>(); //Места которые буду отображать display places

        System.Timers.Timer tPlaces = new System.Timers.Timer(); //Таймер для приёма большей строки поиска


        private void ConnectPlacesRecycler(ref View view) {

            //ImageIcon
            pIcon = (ImageView)view.FindViewById(Resource.Id.placeIcon);

            //ProgressBar
            pProgress = (ProgressBar)view.FindViewById(Resource.Id.placeProgress);

            //TextView
            pickUpText = (TextView)view.FindViewById(Resource.Id.pickUpText);
            pickUpText.Click += PickUpText_Click;

            //EditText
            pickUpLocationEdit = (EditText)view.FindViewById(Resource.Id.pickUpLocationEdit);
            pickUpLocationEdit.AfterTextChanged += PickUpLocationEdit_AfterTextChanged;

            //RecyclerView
            pRecycler = (RecyclerView)view.FindViewById(Resource.Id.placesRecycle);
            pRecycler.SetLayoutManager(new AndroidX.RecyclerView.Widget.LinearLayoutManager(this.Context));

            pAdapter = new PlacesAdapter(ref dPlaces);
            pAdapter.ItemClick += PAdapter_ItemClick;

            pRecycler.SetAdapter(pAdapter);

            tPlaces.Interval = 2000; // Подготавливаю таймер, но ещё не вкючаю его
            tPlaces.Elapsed += TPlaces_Elapsed;
        }

        //Обрабатываю нажатие на поле с адресом рабочего места
        //

        bool isSearchPlace;
        private void PickUpText_Click(object sender, EventArgs e) {
            pickUpLocationEdit.Visibility = ViewStates.Visible;
            centerMarker.Visibility = ViewStates.Gone;  //Прячу маркер

            isSearchPlace = true;   // Указываю, что поиск запущен и на адресное поле было нажатие
        }

        // Принимаю посимвольно текст для поиска
        //

        private void PickUpLocationEdit_AfterTextChanged(object sender, Android.Text.AfterTextChangedEventArgs e) {
            if (isSearchPlace) {    //Если нажал на поле адреса
                SearchPlace();
            }
        }

        // Выполняю поиск мест. Запрашиваю список с именами похожими на введённое. 
        // Так как посимвольный поиск требует много времени (Сервис делает запрос на символ лишь раз в минуту).
        // Поэтому ставлю таймер, чтобы принять как можно больше текста и отправить большой строкой. Так быстрее.
        //

        private void SearchPlace() {

            if (isSearchPlace) {    //Проверяю, запущен ли поиск (Нажал ли на адресное поле)
                pRecycler.Visibility = ViewStates.Visible; //Показываю все элементы для поиска.

                pIcon.Visibility = ViewStates.Gone;

                pProgress.Visibility = ViewStates.Visible;  //Запускаю иконку загрузки
                pProgress.Indeterminate = true;

                tPlaces.AutoReset = false;
                if (tPlaces.Enabled == true) {
                    tPlaces.Stop(); //Выключаю таймер если пишет слишком быстро, чтобы не делать очень много запросов
                }
                tPlaces.Start(); //Включаю таймер, сервис даёт отправлять 1 запрос через 1 секунду
            }
        }


        // Получаю список мест по адресу и отображаю на экране
        //

        object locker = new object();
        private void TPlaces_Elapsed(object sender, ElapsedEventArgs e) {
            lock (locker) {

                dPlaces = mapHelper.GetDisplayPlacesFromSearch(pickUpLocationEdit.Text, 5); ;
                UpdatePlaces();
            }
        }




        //// Обновляю список приглашений на экране
        ////

        private void UpdatePlaces() {
            activity.RunOnUiThread(() => {

                pAdapter.UpdateData(ref dPlaces);
                pAdapter.NotifyDataSetChanged();
            });
        }

        // Принимаю координаты и адрес местоположения.
        //

        LatLng reqLocation; //Местоположение рабочего места

        private void PAdapter_ItemClick(object sender, PlacesAdapter.PlacesAdapterClickEventArgs e) {

            reqLocation.Latitude = dPlaces[e.Position].Latitude;
            reqLocation.Longitude = dPlaces[e.Position].Longitude;

            SetLocationFromPlace(dPlaces[e.Position].DisplayName, reqLocation); //Устанавливаю местоположение
            ClosePlaceSearch();
        }

        // Закрываю поиск по местоположению
        //

        private void ClosePlaceSearch() {
            pRecycler.Visibility = ViewStates.Gone; //Убираю все элементы, которые использовались для выбора места
            pProgress.Visibility = ViewStates.Gone;
            pIcon.Visibility = ViewStates.Visible;
            pickUpLocationEdit.Visibility = ViewStates.Gone;

            dPlaces.Clear();    //Очищаю список отображаемых мест
            isSearchPlace = false;  //Поиск окончен
            pickUpLocationEdit.Text = "";   //Привожу поле для ввода в начальное состояние
            centerMarker.Visibility = ViewStates.Visible;   //Отображаю маркер
        }



        /// Устанавливаю маркер на карте в точку из поиска местоположения
        ///

        string reqAddress;
        public void SetLocationFromPlace(string rAddress, LatLng location) {
            if (uTimer != null) {
                uTimer.Stop();
            }

            activity.RunOnUiThread(() => {

                pickUpText.Text = rAddress;
                pickUpLocationBigText.Text = rAddress;
                reqAddress =rAddress;
                UpdateCamera(location);
            });
        }

        #endregion

        #region MAP UPDATE

        /// Подготавливаю и запускаю всё что связано с картами
        /// после того как они готовы к работе
        /// 
        Mapsui.UI.Android.MapControl mapControl;

        Mapsui.Map map;
        MapFunctionHelper mapHelper;

        System.Timers.Timer uTimer;
        const int UPDATE_TIME = 10000;

        private void CreateMap() {
            mapControl = view.FindViewById<MapControl>(Resource.Id.mapControl);

            map = new Mapsui.Map();
            map.Layers.Add(OpenStreetMap.CreateTileLayer());
            map.RotationLock = true;

            mapControl.Map = map;
            mapControl.Touch += MapControl_Touch;

            mapHelper = new MapFunctionHelper();

            if (CheckLocationPermissions()) {

                // Create your fragment here
                GetCurrentLocation();

                if (uTimer == null) {
                    uTimer = new System.Timers.Timer(UPDATE_TIME);
                    uTimer.Elapsed += Timer_Elapsed;
                }
                uTimer.Start();
            }
        }


        private void MapControl_Touch(object sender, View.TouchEventArgs e) {
            switch(e.Event.Action & MotionEventActions.Mask ) {
                case MotionEventActions.Up:
                    UpdateAddress();
                    break;
                }

            if(uTimer != null) {
                uTimer.Stop();  //Останавливаю обновление местоположения
            }
        }


        Point pPoint = new Point(); //Place Point точка для определения местоположения в широте и долготе
       
        private async void UpdateAddress() {
            pPoint = mapControl.Viewport.Center;
            pPoint = SphericalMercator.ToLonLat(pPoint.X, pPoint.Y);
            reqLocation = new LatLng(pPoint.Y, pPoint.X);

            if (!isSearchPlace) {

                //Запрашиваю адрес из координат
                reqAddress = await mapHelper.GetAddressFromCoordinate(reqLocation);
                pickUpText.Text = reqAddress;
                pickUpLocationBigText.Text = reqAddress;

                
            }
        }









        /// Обновляюсь по сигналу таймера
        /// 

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e) {
            GetCurrentLocation();
        }

        /// Получаю текущее местоположение
        /// 

        LatLng mLastLocation;

        public event EventHandler<LocationUpdatedEventArgs> NewUpdate;
        public class LocationUpdatedEventArgs : EventArgs {
            public LatLng Location { get; set; }
        }

        LatLng newLocation;
        private async void GetCurrentLocation() {

            //Update our LastLocation on the Map
            Location myLocation = await Geolocation.GetLastKnownLocationAsync();

            if (myLocation != null) {

                //Делаю чтобы не выделять несколько раз память под объект mLastLocation
                if(mLastLocation == null) {
                    mLastLocation = new LatLng(myLocation.Latitude, myLocation.Longitude);
                } else {
                    mLastLocation.Latitude = myLocation.Latitude;
                    mLastLocation.Longitude =myLocation.Longitude;
                }

                activity.RunOnUiThread(() => {
                    UpdateCamera(mLastLocation);
                });
                
            }
        }

        /// Показываю текущее местоположение на экране карты
        /// 

        const int zoom = 17; //Уровень приближения камеры к карте
        Point tPoint = new Mapsui.Geometries.Point();

        public void UpdateCamera(LatLng location) {

            tPoint = SphericalMercator.FromLonLat(location.Longitude, location.Latitude);

            //Переношу точку обзора
            mapControl.Navigator.NavigateTo(tPoint, map.Resolutions[zoom]);
            UpdateAddress();
        }


        #endregion

        #region MAP AND LOCATION SERVICES

        /// Определяю текущую позицию по точке центра карты на экране
        /// Без таймера не знаю как сделать, события на окончание изменения карты в Mapsui я не нашёл. Поэтому ждать буду через таймер.
        ///



        




        /// Проверяю разрешения на получение и обработку геолокации
        ///

        const int requestLocationId = 0;

        //Список разрешений которые следует получить 
        readonly string[] permissionGroupLocation =
            { Manifest.Permission.AccessCoarseLocation
            ,Manifest.Permission.AccessFineLocation };

        bool CheckLocationPermissions() {
            bool permissionGranted;

            if ((ActivityCompat.CheckSelfPermission(this.Context, Manifest.Permission.AccessCoarseLocation) != Android.Content.PM.Permission.Granted)
                && (ActivityCompat.CheckSelfPermission(this.Context, Manifest.Permission.AccessFineLocation) != Android.Content.PM.Permission.Granted)) {

                permissionGranted = false;
                RequestPermissions(permissionGroupLocation, requestLocationId);
            } else {
                permissionGranted = true;
            }

            return permissionGranted;
        }


        /// Проверяю результат запроса разрешений
        /// 

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults) {

            if (grantResults.Length < 1) {
                return;
            }

            if (grantResults[0] == (int)Android.Content.PM.Permission.Granted) {
                GetCurrentLocation();
            } else {
                Toast.MakeText(this.Activity, "Не получилось определить местоположение.", ToastLength.Short).Show();
            }
        }

        /// Обрабатываю нажатие на кнопку "Моё местоположение" (справа по центру экрана)
        ///        

        private void MyLocationButton_Click(object sender, System.EventArgs e) {
            GetCurrentLocation();

            if (uTimer == null) {
                uTimer = new System.Timers.Timer(UPDATE_TIME);
                uTimer.Elapsed += Timer_Elapsed;
            }
            uTimer.Start();
        }

        #endregion



        #region CREATE REQUEST

        //Ограничение на приглашения
        const int OFFERS_LIMIT = 4;

        /// Нажатие на кнопку установки местоположения ( "Сюда") открываю карточку заявки
        ///

        int marketWage;
        int marketRate;

        private void LocationSetButton_Click(object sender, System.EventArgs e) {

            if (mLastLocation != null) {

                //Останавливаю обновление местоположения
                if (uTimer != null) {
                    uTimer.Stop();
                }

                //Обновляю рыночную ставку, чтобы дать своевременную информацию
                UpdateMarketRate();

                marketWage = CalculateMarketWage(fromTimeText.Text, toTimeText.Text);

                //Выставляю рыночную зарплату по этому периоду
                promoWageText.Hint = marketWage.ToString();

                //Раскрываю основной вид карточки
                invitationBottomSheetBehavior.State = BottomSheetBehavior.StateExpanded;
            } else {
                Toast.MakeText(activity.BaseContext, "Не получилось установить местоположение устройства", ToastLength.Short).Show();
            }
        }

        /// Обновляю текущую рыночную ставку, чтобы иметь настоящие данные
        /// 

        private void UpdateMarketRate() {
            TcpConnector connector = new TcpConnector(session.ServerIP, session.ServerPort);

            connector.Connect();
            Request request = new Request(session.GetPhone(), "Плательщик", null, 0, null);
            string json = JsonConvert.SerializeObject(request);

            connector.SendData(json);
            json = connector.GetData();
            request = JsonConvert.DeserializeObject<Request>(json);

            marketRate = request.MarketRate;
        }

        //Вычисляю рыночную зарплату 
        private int CalculateMarketWage(string inFromTime, string inToTime) {

            double fullTimeMins = (DateTime.Parse(inToTime) - DateTime.Parse(inFromTime)).TotalMinutes;
            double fullTimeHours = fullTimeMins / 60;

            //Считаю зарплату по рыночной цене для текущих рабочих часов
            double mWage = Convert.ToDouble(marketRate) * fullTimeHours;
            mWage = Math.Floor((mWage / 10) * 10);

            return Convert.ToInt32(mWage);
        }

        /// Меняю статус приглашения, те сравниваю с рыночной зарплатой
        /// 
        private void StatusChange() {
            int wage;
            if (int.TryParse(promoWageText.Text, out wage)) {

                int mStatus = wage - marketWage;
                int absStatus = Math.Abs(mStatus);
                if (mStatus >= 0) {

                    statusText.Text = "выше";
                    statusText.SetTextColor(Color.Green);

                    if (absStatus > 99) {
                        statusWageText.Text = "99+";
                    } else {
                        statusWageText.Text = absStatus.ToString();
                    }
                    statusWageText.SetTextColor(Color.Green);
                } else {
                    if (mStatus < 0) {
                        statusText.Text = "ниже";
                        statusText.SetTextColor(Color.OrangeRed);

                        if (absStatus > 99) {
                            statusWageText.Text = "99+";
                        } else {
                            statusWageText.Text = absStatus.ToString();

                        }
                        statusWageText.SetTextColor(Color.OrangeRed);
                    }
                }


                int moneyStatus = Math.Abs(mStatus % 10);

                if ((moneyStatus > 1) && (moneyStatus < 5) && !((absStatus > 10) && (absStatus < 20))) {
                    moneyText.Text = " рубля";
                } else {
                    if ((moneyStatus == 1) && (absStatus != 11)) {
                        moneyText.Text = " рубль";
                    } else {
                        moneyText.Text = " рублей";
                    }
                }
            }
        }

        /// Вызываю замену статуса приглашения после каждого изменения оплаты
        ///

        private void PromoFareText_AfterTextChanged(object sender, Android.Text.AfterTextChangedEventArgs e) {
            StatusChange();
        }

        /// Отправляю событие открыть диалог с выбором времени для установки начала работы
        ///

        // FLAG
        char fromOrTo;

        public event EventHandler<RequestTimeEventArgs> SetRequestTime;
        public class RequestTimeEventArgs : EventArgs {
            public char FromOrTo { get; set; }
        }

        private void FromTimeText_Click(object sender, System.EventArgs e) {
            fromOrTo = 'f';
            SetRequestTime?.Invoke(this, new RequestTimeEventArgs { FromOrTo = fromOrTo });
        }

        /// Отправляю событие открыть диалог с выбором времени для установки конца работы
        ///

        private void ToTimeText_Click(object sender, System.EventArgs e) {
            fromOrTo = 't';
            SetRequestTime?.Invoke(this, new RequestTimeEventArgs { FromOrTo = fromOrTo });
        }

        /// Устанавливаю выбранное из диалога время в соответствующее представление
        /// 

        public void TimeSet(ref string time) {
            bool isCorrect = true;
            DateTime f = DateTime.Parse(fromTimeText.Text);
            DateTime t = DateTime.Parse(toTimeText.Text);
            DateTime inTime = DateTime.Parse(time);

            switch (fromOrTo) {
                case 'f':
                    if ((inTime < t)) {
                        fromTimeText.Text = time;
                    } else {
                        isCorrect = false;
                    }
                    break;
                case 't':
                    if ((inTime > f)) {
                        toTimeText.Text = time;
                    } else {
                        isCorrect = false;
                    }
                    break;
            }
            if (isCorrect) {

                marketWage = CalculateMarketWage(fromTimeText.Text, toTimeText.Text);
                promoWageText.Hint = marketWage.ToString(); //Выставляю рыночную зарплату по этому периоду
                StatusChange();
            } else {
                Toast.MakeText(activity.BaseContext, "Ошибка в рабочем времени.", ToastLength.Short).Show();
            }
        }

        /// Устанавливаю дату предложения
        /// 

        DateTime dRequest = DateTime.Today;
        private void DateText_Click(object sender, EventArgs e) {
            switch (dateText.Text) {
                case "Сегодня":
                    dRequest = DateTime.Today.AddDays(1);
                    dateText.Text = "Завтра";
                    break;
                case "Завтра":
                    dRequest = DateTime.Today.AddDays(2);
                    dateText.Text = "Послезавтра";
                    break;
                case "Послезавтра":
                    dRequest = DateTime.Today;
                    dateText.Text = "Сегодня";
                    break;
            }
        }

        /// Нажатие на кнопку заявки промоутера
        ///

        FullOfferInfo offer;

        private void RequestPromoButton_Click(object sender, System.EventArgs e) {
            if (session.Counter < OFFERS_LIMIT) {

                BaseOfferInfo baseRequest = new BaseOfferInfo();
                baseRequest.PickUpLat = reqLocation.Latitude;
                baseRequest.PickUpLng = reqLocation.Longitude;

                if (int.TryParse(promoWageText.Text, out int arg)) {

                    baseRequest.Wage = arg;
                } else {
                    Toast.MakeText(activity, "Укажите заработок", ToastLength.Short).Show();
                    return;
                }

                baseRequest.FromTime = fromTimeText.Text;
                baseRequest.ToTime = toTimeText.Text;
                baseRequest.Date = dRequest.ToString(CultureInfo.InvariantCulture);

                offer = new FullOfferInfo(baseRequest);

                offer.Documents = documentsCheck.Checked;
                offer.SpecificDocuments = specificDocumentsCheck.Checked;
                offer.SpecificView = specificExteriorCheck.Checked;

                offer.PickUpAddress = reqAddress;
                offer.Comment = commentText.EditText.Text;
                offer.PayMentMethod = "Наличные";
                offer.Timestamp = DateTime.Now;
                offer.Status = "Жду";

                offer.CustomerContact = session.GetPhone();
                SendToServer(ref offer);

                ResetInterface();
            } else {
                Toast.MakeText(activity.BaseContext
                    , "Мы уже отправили 4 предложения из 4-х возможных за сегодня."
                    , ToastLength.Short).Show();
            }
        }

        /// Отправляю данные на сервер
        ///
        private void SendToServer(ref FullOfferInfo offer) {
            TcpConnector connector = new TcpConnector(session.ServerIP, session.ServerPort);

            connector.Connect();
            string content = JsonConvert.SerializeObject(offer);
            Request request = new Request(session.GetPhone(), "Плательщик", null, 0, content);
            string json = JsonConvert.SerializeObject(request);

            connector.SendData(json);
            connector.GetData();
        }

        //// Сбрасываю интерфейс и подготавливаю к новой заявке
        ///

        private void ResetInterface() {
            session.IncreaseCounter();
            session.Save();
            Toast.MakeText(activity.BaseContext
                , $"Мы отправили сегодня {session.Counter} из 4-х возможных предложений."
                , ToastLength.Short).Show();

            locationSetButton.Visibility = ViewStates.Visible;
            invitationBottomSheetBehavior.State = BottomSheetBehavior.StateHidden;

            requestPromoButton.Visibility = ViewStates.Visible;

            ClosePlaceSearch();
            GetCurrentLocation();

            if (uTimer == null) {
                uTimer = new System.Timers.Timer(UPDATE_TIME);
                uTimer.Elapsed += Timer_Elapsed;
            }
            uTimer.Start();
        }

        #endregion



    }
}
