﻿//// Фрагмент отображает всю официальну дополнительную информацию,
//// включая ссылки для связи.
///

using System;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace JosephStalin.MainFragments {
    public class HelpFragment : Android.Support.V4.App.Fragment {

        #region CREATE FRAGMENT AND CONNECT VIEWS

        /// Создаю фрагмент
        ///
        
        public override void OnCreate(Bundle savedInstanceState) {
            base.OnCreate(savedInstanceState);

            // Create your fragment here   
        }

        /// Загружаю в представление фрагмента макет интерфейса
        /// 
        

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            // Use this to return your custom view for this Fragment
            View view =inflater.Inflate(Resource.Layout.help, container, false);

            ConnectViews(ref view);
			return view;
        }

        /// Связываю представления с макетом
        /// 

        Button salaryButton;
        ImageButton communityButton;

        public event EventHandler OpenSalary;
        public event EventHandler OpenCommunity;

        void ConnectViews(ref View view) {
            salaryButton = (Button)view.FindViewById(Resource.Id.salaryButton);
            communityButton = (ImageButton)view.FindViewById(Resource.Id.communityButton);

            salaryButton.Click += (o, e) => {
                OpenSalary?.Invoke(this, new EventArgs());
            };
            
            communityButton.Click += (o, e) => {
                OpenCommunity?.Invoke(this, new EventArgs());    
            };
        }

        #endregion
    }
}
