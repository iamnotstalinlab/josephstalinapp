﻿// Помощник который непосредственно (между ним и данными нет других обрабочиков) обрабатывает данные от Гугла

using System.Linq;
using System.Threading.Tasks;
using Android.Gms.Maps.Model;
using Nominatim.API.Geocoders;
using Nominatim.API.Models;
using System.Collections.Generic;
using static JosephStalin.Adapter.PlacesAdapter;

namespace JosephStalin.Helpers {
    public class MapFunctionHelper {

        private List<GeocodeResponse> Places { get; set; }
        

        // Создаю объект помощника
        //

        public MapFunctionHelper() {
            Places = new List<GeocodeResponse>();
            DisplayPlaces = new List<DisplayPlace>();
        }

        // Получаю полный список мест
        //

        private void GetPlacesFromSearch(string search, int maxCount) {

            Places.Clear();
            ForwardGeocoder fGeocoder = new ForwardGeocoder(); //Создаю запрос
            var request = fGeocoder.Geocode(new ForwardGeocodeRequest {
                queryString = search,
                City = "Moscow",
                Country = "Russia",
                LimitResults = 5,
                State = "Moscow",
                CountryCodeSearch = "RU"
            }); //Отправляю запрос и получаю ответ

            if (request.Result.Length > 0) {

                GeocodeResponse[] results = request.Result.ToArray();
                int count = 0;
                foreach (GeocodeResponse resp in results) {
                    if (count < maxCount) {
            
                        Places.Add(resp);
                        ++count;
                    }
                }
            }
        }




        //Получаю список мест для отображения
        //

        private List<DisplayPlace> DisplayPlaces { get; set; }

        public List<DisplayPlace> GetDisplayPlacesFromSearch(string search, int maxCount) {
            DisplayPlaces.Clear();

            GetPlacesFromSearch(search, maxCount);  //Получаю полный список мест
            foreach (GeocodeResponse place in Places) {
                DisplayPlace dPlace = new DisplayPlace(place.DisplayName, place.IconURL);
                dPlace.Latitude = place.Latitude;
                dPlace.Longitude = place.Longitude;

                DisplayPlaces.Add(dPlace);
            }
            return DisplayPlaces;
        }

        // Отправляю запрос на перевод координат в адрес и выполняем обработку полученных данных по поиску адреса из координат
        //

        public async Task<string> GetAddressFromCoordinate(LatLng position) {
            
            ReverseGeocoder reverse = new ReverseGeocoder();
            var response = reverse.ReverseGeocode(new ReverseGeocodeRequest {
                Longitude = position.Longitude,
                Latitude = position.Latitude,

                BreakdownAddressElements = true,
                ShowExtraTags = true,
                ShowAlternativeNames = true,
                ShowGeoJSON = true
            });
            response.Wait();    //Жду ответ от сервиса

            if(response.Result.PlaceID > 0) {
                return response.Result.DisplayName; 
            }

            return null;
        }


        
    }
}
