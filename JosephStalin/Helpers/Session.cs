﻿// Объект который обеспечивает сохранность сессии и данных
//

using System;
using Android.App;
using Android.Content;

namespace JosephStalin.Helpers {
    public class Session {

        public string ServerIP { get; set; }
        public int ServerPort { get; set; }

        //Счётчик отправленных сегодня предложений
        public int Counter {
            get {
                return counter;
            }
        }


        #region SAVE SESSION

        //Создаю файл для сохранения
        ISharedPreferences preferences = Application.Context.GetSharedPreferences("userinfo ", FileCreationMode.Private);
        ISharedPreferencesEditor editor; //Создаю объект который будет записывать обновления


        public Session() {
            editor = preferences.Edit(); //Связываю записывающий объект с файлом в который будет писать

            // Закрываю данные адреса и порта. Ставить свои.
            ServerIP ="12121212121212";
            ServerPort= 12121212121;
        }

        // Открываю сеанс вместе с приложением
        //

        //Счётчик отправленных сегодня предложений
        int counter;
        public void Open() {

            //Получаю дату предыдущего сеанса
            string sToday = preferences.GetString("Дата последней сессии", "");

            //Если прежний сеанс был не сегодня, обнуляю счётчик отправлений
            if (sToday == DateTime.Now.Date.ToString("dd/MM/YYYY")) {
                counter = preferences.GetInt("Предложений за сегодня", 0);
            }
        }


        // Сохраняю сеанс
        //

        public void Save() {
            

            editor.PutString("Дата последней сессии", DateTime.Now.Date.ToString("dd/MM/YYYY")); // Подготавливаю дату для записи
            editor.PutInt("Предложений за сегодня", counter); //Подготавливаю счётчик для записи
            editor.Apply(); //Записываю всё что подготовил
        }
        #endregion 

        // Повышаю счётчик отправлений
        //

        public void IncreaseCounter() {
            ++counter;
        }

        // Получаю телефонный номер 
        //

        public string GetPhone() {
            return preferences.GetString("Номер", "");
        }

        // Сохраняю телефонный номер
        //

        public void SavePhone(string phone) {
            editor.PutString("Номер", phone);
            editor.Apply();
        }


        public void SavePin(string pin) {
            editor.PutString("Пин", pin);
            editor.Apply();
        }

        public string GetPin() {
            return preferences.GetString("Пин", "");
        }

    }
}
