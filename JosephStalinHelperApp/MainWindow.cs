﻿using System;
using System.Globalization;
using System.Linq;
using Gtk;
using JosephStalinHelperApp.DataModels;
using JosephStalinHelperApp.EventListeners;
using Newtonsoft.Json;
using Nominatim.API.Models;
using Nominatim.API.Geocoders;


public partial class MainWindow : Gtk.Window {

    //Connection to Server закрываю данные адреса и порта. Ставить свои.
    static int port = 121212121; //Порт 
    static string address = "121212121212"; //Адрес

    GeocodeResponse[] results;

    DateTime fromT;
    DateTime toT;

    double lat;
    double lng;
    int wage;
    int phone;
    string offAddress;
    DateTime dateRequest;

    Gdk.Color blue = new Gdk.Color();
    Gdk.Color black = new Gdk.Color();

    
    
    public MainWindow() : base(Gtk.WindowType.Toplevel) {
        Build();


        Gdk.Color.Parse("#0000ff", ref blue);
        Gdk.Color.Parse("black", ref black);

        dateRequest = DateTime.Today;
        btnToday.Child.ModifyFg(StateType.Normal, blue);
    }



    private void PlacesAutoComplete() {
        foreach (Gtk.Widget element in placesBox) {
            placesBox.Remove(element);
        }

        try {

            ForwardGeocoder x = new ForwardGeocoder();
            var r = x.Geocode(new ForwardGeocodeRequest {
                queryString = placesEntry.Text
                ,
                City = "Moscow"
                ,
                County = "RU"
            });


            //Если запрос даёт ответ
            if (r.Result.Length > 0) {

                int nameBtn = 0;
                results = r.Result.ToArray();
                foreach (GeocodeResponse resp in results) {

                    if(nameBtn < 5) { // Делаю это условие чтобы не было больше 5 вариантов. А то получается куча кнопок.
                        string btnLbl = $"{resp.DisplayName} ({resp.ClassType})";

                        Button button = new Button(btnLbl);
                        button.Name = nameBtn.ToString();
                        button.Clicked += Button_Clicked;
                        placesBox.Add(button);
                        ++nameBtn;

                    }
                }
            }

            placesBox.ShowAll();


        } catch (Exception ex) {
            string s = ex.Message;
        }
    }



    private void Button_Clicked(object sender, EventArgs e) {
        Button btn = (Button)sender;
        int i = int.Parse(btn.Name.ToString());

        lat = results[i].Latitude;
        lng = results[i].Longitude;

        placesEntry.Text = btn.Label;

        foreach (Gtk.Widget element in placesBox) {
            placesBox.Remove(element);
        }
    }

    protected void OnDeleteEvent(object sender, DeleteEventArgs a) {
        Application.Quit();
        a.RetVal = true;
    }

    protected void PlacesEntry_Changed(object sender, EventArgs e) {
        if (placesEntry.Text != String.Empty) {

            PlacesAutoComplete();
        }
    }



    //Отправляю предложение на сервер
    //

    protected void Clicked(object sender, EventArgs e) {
        BaseOfferInfo info = new BaseOfferInfo();

        if (CheckData()) {
            info.Date = dateRequest.ToString(CultureInfo.InvariantCulture);
            info.Wage = wage;

            info.FromTime = fromT.ToString("HH:mm");
            info.ToTime = toT.ToString("HH:mm");

            info.PickUpLat = lat;
            info.PickUpLng = lng;


            FullOfferInfo offer = new FullOfferInfo(info);
            offer.PickUpAddress = offAddress;
            offer.Comment = commentEdit.Buffer.Text;
            offer.Timestamp = DateTime.Now;
            offer.CustomerContact = contactEn.Text;

            perviousTime.Text = $"Прошлое заполнение: " +
                $"{DateTime.Now.Hour.ToString()}: " +
                $"{DateTime.Now.Minute.ToString()} " +
                $"{DateTime.Now.ToString("dd/MMM/yyyy")}";

            string content = JsonConvert.SerializeObject(offer);
            Request request = new Request("Плательщик","Оператор","Приглашение", 0, content);

            SendToServer(ref request);
        }
    }





    private bool CheckData() {
        string msg = "Не смог преобразовать:";
        bool isCorrect = true;

        if (lat == 0 || lng == 0) {
            msg += "\n Местоположение";
            isCorrect = false;
        }

        if (DateTime.TryParse(fromTime.Text, out fromT) == false) {
            msg += "\n Время начала";
            isCorrect = false;

        }
        if (DateTime.TryParse(ToTime.Text, out toT) == false) {
            msg += "\n Время окончания";
            isCorrect = false;
        }

        if (int.TryParse(wageEdit.Text, out wage) == false) {
            msg += "\n Зарплату";
            isCorrect = false;
        }

        if (fromT > toT) {
            msg += "\n Начало позже чем конец работы";
            isCorrect = false;
        }

        // Проверяю телефон
        if (string.IsNullOrEmpty(contactEn.Text)) {

            msg += "\n Связь";
            isCorrect = false;
        }
        offAddress = placesEntry.Text;

        if (!isCorrect) {
            ShowMessage(msg);
        }

        return isCorrect;
    }


    private void SendToServer(ref Request request) {
        TcpConnector connector = new TcpConnector(address, port);
        connector.Connect();

        string json = JsonConvert.SerializeObject(request);

        connector.SendData(json);
        connector.GetData();
    }


    private void ShowMessage(string message) {
        MessageDialog msgDialog = new MessageDialog(null,
            DialogFlags.DestroyWithParent,
            MessageType.Error,
            ButtonsType.Ok,
            message);

        msgDialog.Run();
        msgDialog.Destroy();
    }


    protected void btnResetClicked(object sender, EventArgs e) {
        Reset();
    }

    private void Reset() {
        fromT = new DateTime();
        toT = new DateTime();

        lat = 0;
        lng = 0;
        wage = 0;
        offAddress = null;

        fromTime.Text = "";
        ToTime.Text = "";
        wageEdit.Text = "";
        contactEn.Text = "";
        placesEntry.Text = "";
        commentEdit.Buffer.Text = "";

        dateRequest = DateTime.Today;
        ResetColorButtons();
        btnToday.Child.ModifyFg(StateType.Normal, blue);
    }


    private void ResetColorButtons() {
        btnToday.Child.ModifyFg(StateType.Normal, black);
        btnTomorrow.Child.ModifyFg(StateType.Normal, black);
        btnAfterTomorrow.Child.ModifyFg(StateType.Normal, black);
    }

    protected void btnTodayClicked(object sender, EventArgs e) {

        dateRequest = DateTime.Today;
        ResetColorButtons();

        btnToday.Child.ModifyFg(StateType.Normal, blue);
    }

    protected void btnTomorrowClicked(object sender, EventArgs e) {
        dateRequest = DateTime.Today.AddDays(1);
        ResetColorButtons();

        btnTomorrow.Child.ModifyFg(StateType.Normal, blue);
    }

    protected void btnAfterTomorrowClicked(object sender, EventArgs e) {
        dateRequest = DateTime.Today.AddDays(2);
        ResetColorButtons();

        btnAfterTomorrow.Child.ModifyFg(StateType.Normal, blue);
    }



    // Открываю окно настроек при нажатии на кнопку "Настройки"
    //
    
    Entry sEntry;
    Window wSettings =new Window("Настройки");

    

    protected void btnSettings_Clicked(object sender, EventArgs e) {


        Label sLabel = new Label("Введите контакт участника в поле ниже");
        sEntry =new Entry();

        Button sBtnAdmitUser = new Button("Допустить");
        sBtnAdmitUser.Clicked += SBtnAdmitUser_Clicked;
        sBtnAdmitUser.Child.ModifyFg(StateType.Normal, blue);
        
        Button sBtnExclude =new Button("Исключить");
        sBtnExclude.Child.ModifyFg(StateType.Normal, black);
        sBtnExclude.Clicked += SBtnExclude_Clicked;

        VBox sBox =new VBox();
        sBox.Add(sLabel);
        sBox.Add(sEntry);
        sBox.Add(sBtnAdmitUser);
        sBox.Add(sBtnExclude);

        wSettings.Add(sBox);

        //Отображаю окно по центру экрана
        int sWidth;
        int sHeight;
        Gdk.Screen screen = this.Screen;
        wSettings.GetSize(out sWidth, out sHeight);

        wSettings.Move((screen.Width /2) -(sWidth /2), (screen.Height /2) -(sHeight /2));
        wSettings.ShowAll();
    }

    private void SBtnExclude_Clicked(object sender, EventArgs e) {
        if (!string.IsNullOrEmpty(sEntry.Text)) {
            Request req =new Request("Оператор", "Оператор", "Исключить",0, sEntry.Text);
            SendToServer(ref req);
        } else {
            ShowMessage("Не смог определить контакт.");
        }   
    }

    private void SBtnAdmitUser_Clicked(object sender, EventArgs e) {
        if(!string.IsNullOrEmpty(sEntry.Text)){

            Request req =new Request("Оператор", "Оператор", "Допустить", 0, sEntry.Text);
            SendToServer(ref req);
        } else {
            ShowMessage("Не смог определить контакт.");
        }
    }
}
