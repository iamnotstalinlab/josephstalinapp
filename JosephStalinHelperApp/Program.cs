﻿using System;
using Gtk;

namespace JosephStalinHelperApp {
    class MainClass {


        public static void Main(string[] args) {
            int wHeight;
            int wWidth;

            Application.Init();
            MainWindow win = new MainWindow();

            Gdk.Screen screen = win.Screen;

            win.GdkWindow.GetSize(out wWidth, out wHeight);
            win.Move((screen.Width /2) - (wWidth/2), (screen.Height /2) - (wHeight /2));

            win.Show();
            Application.Run();
        }

    }
}
