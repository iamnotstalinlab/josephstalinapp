﻿
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Views;
using Android.Widget;
using JosephStalinForTheProletarians.DataModels;

namespace JosephStalinForTheProletarians.Adapter {
    public class MarketAdapter : RecyclerView.Adapter{

        public event EventHandler<MarketAdapterClickEventArgs> ItemButtonClick;

        public List<FullOfferInfo> Items {get;set; }
        public int MarketRate {get;set; } //mRate - среднерыночная ставка Market

        //tWage - заработок текущего предложения This
        //eWage - ожидаемый заработок (на основе рыночной ставки) Earnings
        
        int eWage;
        int tWage;

        //Colors
        Color mainColor;
        Color secondColor;

        //Создаю адаптер и наполняю его первоначальными данными
        //

        public MarketAdapter(ref List<FullOfferInfo> Data, ref int marketRate) {

            Items = Data;
            MarketRate = marketRate;
        }

        //Создаю карточку предложения
        //

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.mOffer, parent, false);

            var vh = new MarketAdapterViewHolder(itemView, OnNextStepClick);

            mainColor = itemView.Resources.GetColor(Resource.Color.jsMain);
            secondColor = itemView.Resources.GetColor(Resource.Color.jsSecond);

            Log.WriteLine(LogPriority.Warn, "VIEW HOLDER", "ON CREATE");
            return vh;

        }

        public override int ItemCount => Items.Count();


        // Привязываю карточку приглашения к данным из списка
        //

        //BindViewHolder
        public override void OnBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

            var holder = viewHolder as MarketAdapterViewHolder;
            if (Items != null) {

                RefreshHolderData(ref holder, position);
                RefreshStatus(ref holder, position);
                BrightMode(ref holder);
            }
            Log.WriteLine(LogPriority.Warn, "BIND HOLDER", "ON BIND ITEM  " + position);
        }

        void OnNextStepClick(MarketAdapterClickEventArgs arg) => ItemButtonClick?.Invoke(this, arg);

        

        public class MarketAdapterViewHolder : RecyclerView.ViewHolder {

            public TextView statusText { get; set; }
            public TextView statusWageText { get; set; }
            public TextView moneyText { get; set; }


            public TextView dateText { get; set; }
            public TextView distanceText { get; set; }
            public TextView wageText { get; set; }
            public TextView fromTimeText { get; set; }
            public TextView toTimeText { get; set; }

            public LinearLayout statusLayout { get; set; }

            public ImageButton nextButton { get; set; }
            public ImageButton backButton { get; set; }

            // Создаю карточку предложения и соединяю все представления
            //

            public MarketAdapterViewHolder(View itemView,
                Action<MarketAdapterClickEventArgs> buttonClickListener) : base(itemView) {

                //marketRate = rate;
                //Layouts
                statusLayout = (LinearLayout)itemView.FindViewById(Resource.Id.statusLayout);

                //TextView
                statusText = (TextView)itemView.FindViewById(Resource.Id.statusText);
                statusWageText = (TextView)itemView.FindViewById(Resource.Id.statusWageText);
                dateText = (TextView)itemView.FindViewById(Resource.Id.dateText);
                distanceText = (TextView)itemView.FindViewById(Resource.Id.distanceText);

                wageText = (TextView)itemView.FindViewById(Resource.Id.wageText);
                fromTimeText = (TextView)itemView.FindViewById(Resource.Id.fromTimeText);
                toTimeText = (TextView)itemView.FindViewById(Resource.Id.toTimeText);
                moneyText = (TextView)itemView.FindViewById(Resource.Id.moneyText);

                //Buttons
                nextButton = (ImageButton)itemView.FindViewById(Resource.Id.nextButton);
                backButton = (ImageButton)itemView.FindViewById(Resource.Id.backButton);

                nextButton.Click += (sender, e) => buttonClickListener(new MarketAdapterClickEventArgs {
                    View = itemView,
                    Position = AdapterPosition
                });

                backButton.Click += (sender, e) => buttonClickListener(new MarketAdapterClickEventArgs {
                    View = itemView,
                    Position = AdapterPosition
                });

            }
        }

        public class MarketAdapterClickEventArgs : EventArgs {
            public View View { get; set; }
            public Int32 Position { get; set; }
        }



        #region SetViewMode

        //Установливаю яркий режим (сейчас не используется)
        //
        public void BrightMode(ref MarketAdapterViewHolder holder) {

            holder.dateText.SetTextColor(secondColor);
            holder.dateText.SetTypeface(Typeface.Default, TypefaceStyle.Normal);

            holder.fromTimeText.SetTextColor(mainColor);
            holder.fromTimeText.SetTypeface(Typeface.Default, TypefaceStyle.Bold);

            holder.toTimeText.SetTextColor(mainColor);
            holder.toTimeText.SetTypeface(Typeface.Default, TypefaceStyle.Bold);

            holder.wageText.SetTextColor(mainColor);
            holder.wageText.SetTypeface(Typeface.Default, TypefaceStyle.Bold);
        }

        //Установливаю блёклый режим (сейчас не используется)
        //

        private void DimMode(ref MarketAdapterViewHolder holder) {

            holder.dateText.SetTextColor(mainColor);
            holder.dateText.SetTypeface(Typeface.Default, TypefaceStyle.Bold);

            holder.fromTimeText.SetTextColor(secondColor);
            holder.fromTimeText.SetTypeface(Typeface.Default, TypefaceStyle.Normal);

            holder.toTimeText.SetTextColor(secondColor);
            holder.toTimeText.SetTypeface(Typeface.Default, TypefaceStyle.Normal);

            holder.wageText.SetTextColor(secondColor);
            holder.wageText.SetTypeface(Typeface.Default, TypefaceStyle.Normal);
        }

        // Прячу строку статуса (сейчас не использую)
        //

        private void HideStatus(ref MarketAdapterViewHolder holder) {
            holder.statusLayout.Visibility = ViewStates.Gone;
        }

        // Отображаю строку статуса (сейчас не использую)
        //

        public void ShowStatus(ref MarketAdapterViewHolder holder) {
            holder.statusLayout.Visibility = ViewStates.Visible;
        }

        // Отображаю кнопку влево (сейчас не использую)
        //

        void ReverseButtonMode(ref MarketAdapterViewHolder holder) {
            holder.backButton.Visibility = ViewStates.Visible;
            holder.nextButton.Visibility = ViewStates.Gone;
        }

        // Отображаю кнопку вправо (сейчас функцию не использую)
        //

        void NormalButtonMode(ref MarketAdapterViewHolder holder) {
            holder.backButton.Visibility = ViewStates.Gone;
            holder.nextButton.Visibility = ViewStates.Visible;
        }
        #endregion


        // Обновляю данные в карточке предложения
        //

        private void RefreshHolderData(ref MarketAdapterViewHolder holder, int position) {

            holder.dateText.Text = GetTranslateDate(position);
            holder.distanceText.Text = Items[position].Radius.ToString();

            holder.wageText.Text = Items[position].Wage.ToString();
            holder.fromTimeText.Text = Items[position].FromTime;
            holder.toTimeText.Text = Items[position].ToTime;
        }


        /// Перевожу дату чисел в строковой формат
        /// 

        private string GetTranslateDate(int position) {

            //Андроид мудак и не понимает, что я хочу дату через слеш, поэтому приходится объяснять этому барану и переделывать всё самому 
            //А потом до разработчиков допрёт, что они наделали хуйни и я получу какую-нибудь ошибку) Ахуенно бля.
            
            DateTime sToday = DateTime.Today;
            DateTime sTomorrow = DateTime.Today.AddDays(1);
            DateTime sAfTomorrow = DateTime.Today.AddDays(2);

            DateTime dRequest = DateTime.Parse(Items[position].Date, CultureInfo.InvariantCulture);

            string retDate = "Сегодня";
            if(dRequest == sToday) {
                retDate = "Сегодня";
            }

            if(dRequest == sTomorrow) {
                retDate = "Завтра";
            }

            if(dRequest == sAfTomorrow) {
                retDate = "Послезавтра";
            }

            return retDate;
        }


        // Обновляю строку статуса предложения
        //

        private void RefreshStatus(ref MarketAdapterViewHolder holder, int position) {
            tWage = Items[position].Wage;

            eWage = CalculateEarningWage(MarketRate, Items[position]);
            WriteStatusText(ref holder, ref position);
            ShowStatus(ref holder);
        }


        // Вычисляю предполагаемую зарплату (используется в статусе)
        //

        private int CalculateEarningWage(int mRate, FullOfferInfo offer) {

            //Собираю указанное в приглашении трудовое время
            DateTime fromTime = DateTime.Parse(offer.FromTime);
            DateTime toTime = DateTime.Parse(offer.ToTime);

            //Вычисляю время труда в часах
            double labHours = (toTime - fromTime).TotalMinutes /60;

            //Считаю заработок, который положен сейчас на рынке
            double mWage = mRate * labHours;

            //Округляю
            mWage = Math.Floor((mWage / 10) * 10);
            return Convert.ToInt32(mWage);
        }


        // Записываю текст статуса в представления
        //

        private void WriteStatusText(ref MarketAdapterViewHolder holder, ref int position) {

            //sWage - статус зарплаты. 
            int sWage = tWage - eWage;
            int absStatus = Math.Abs(sWage);

            if (sWage >= 0) {

                holder.statusText.Text = "выше";
                holder.statusText.SetTextColor(Color.Green);

                if (absStatus > 99) {
                    holder.statusWageText.Text = "99+";

                } else {
                    holder.statusWageText.Text = absStatus.ToString();
                }
                holder.statusWageText.SetTextColor(Color.Green);
            } else {
                if (sWage < 0) {
                    holder.statusText.Text = "ниже";
                    holder.statusText.SetTextColor(Color.OrangeRed);

                    if (absStatus > 99) {
                        holder.statusWageText.Text = "99+";
                    } else {
                        holder.statusWageText.Text = absStatus.ToString();
                    }
                    holder.statusWageText.SetTextColor(Color.OrangeRed);
                }
            }

            WriteMoneyText(holder, ref position, ref sWage, ref absStatus);

        }

        // Записываю грамотный текст относительно суммы в поле денег
        //

        private void WriteMoneyText(MarketAdapterViewHolder holder, ref int position, ref int sWage, ref int absStatus) {
            int moneyStatus = Math.Abs(sWage % 10);

            if ((moneyStatus > 1) && (moneyStatus < 5) && !((absStatus > 10) && (absStatus < 20)) && (absStatus < 99)) {
                holder.moneyText.Text = " рубля";
            } else {
                if ((moneyStatus == 1) && (absStatus != 11) && (absStatus < 99)) {
                    holder.moneyText.Text = " рубль";
                } else {
                    holder.moneyText.Text = " рублей";
                }
            }
        }


    }
}
