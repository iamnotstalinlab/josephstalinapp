﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Android.Util;
using JosephStalinForTheProletarians.DataModels;
using Newtonsoft.Json;

namespace JosephStalinForTheProletarians.EventListeners {
    public class TcpConnector {
        private Socket socket; //Сокет который обрабатывает запросы и выполняет с ними действия
        private IPEndPoint ipPoint; //Точка к которой буду соединяться

        private int bytes; //Количество пришедших байтов
        List<byte> buffer = new List<byte>(); //Буфер где мы временно размещаем пришедшие данные

        int pkgLength = -1; //Длина пакета посылки
        byte[] tempBuf = new byte[104]; //Массив памяти для приёма данных с сокета

        private StringBuilder builder;//Объект который собирает нам запрос
        private String data; //Сам объект запроса

        public event EventHandler<ConnectorExceptionEventArgs> ConnectorException; // Если будет ошибка, то отправлю это событие
        public class ConnectorExceptionEventArgs : EventArgs {
            public string Message { get; set; } // В событии сообщу причину ошибки
        }

        public bool Connected {
            get {
                return socket.Connected;
            }
        }

        /// Связной уже пришёл и здесь обрабатываю его посещеение
        /// 

        public TcpConnector(Socket socket) {

            //Здесь связной в роли гостя
            this.socket = socket;
            builder = new StringBuilder();
        }


        /// Подготавливаю связного на поход к серверу.
        /// Говорю ему адрес и даю пустую строковую переменную stringBuilder


        public TcpConnector(string ipAddress, int port) {
            builder = new StringBuilder();

            //Здесь связной выступает в роли посыльного
            ipPoint = new IPEndPoint(IPAddress.Parse(ipAddress), port);
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        /// Проверяю жив ли сейчас сервер
        ///


        private bool isServer() {
            if (socket.Poll(1000, SelectMode.SelectWrite)) {
                return true;
            } else {
                string message = "Мы не смогли подключиться к серверу. Попробуем чуть позже.";
                ConnectorException?.Invoke(this, new ConnectorExceptionEventArgs { Message = message });
                return false;
            }
        }

        /// Подклюсь к удалённому серверу (те хосту)
        ///

        public void Connect() {
            socket.Connect(ipPoint);
        }

        /// Очищаю пространство памяти для приёма данных
        /// 

        private void ClearBuffer() {
            pkgLength = -1;
            buffer.Clear();
            builder.Clear();
        }

        /// Принимаю пакет данных
        /// 

        public string GetData() {

            //Если есть связь с сервером
            if (isServer()) {

                //Подготавливаю память и переменные к приёму
                ClearBuffer();
                do {
                    //Принимаю порцию байтов из сокета 
                    bytes = socket.Receive(tempBuf); //В bytes записывается количество байт в порции
                    buffer.AddRange(tempBuf.Take(bytes)); //Помещаю полученную порцию байт в список памяти List 

                    if ((pkgLength == -1) && (bytes > sizeof(int))) { //Проверяю знаем ли мы длину пакета
                        pkgLength = BitConverter.ToInt32(buffer.ToArray(), 0); //Если не знаем ещё, то читаю из первых 4х байт
                    }
                    ////    Условие while, очень хитрое:
                    ///     buffer.Count всегда >= 0, pkgLength либо -1 либо >0
                    ///     поэтому while работает правильно при любых пакетах данных
                } while (buffer.Count < pkgLength); //Принимаю все порции пакета

                if(buffer.Count() > sizeof(int)) {
                    buffer.RemoveRange(0, sizeof(int)); //Удаляю из памяти блок содержащий длину пакета
                }
                builder.Append(Encoding.Unicode.GetString(buffer.ToArray(), 0, buffer.Count)); //Записываю байты в строку

                // Возвращаю из функции строку данных
                data = builder.ToString();
                return data;
            } //Если сервера нет то возвращаю null
            return null;
        }

        //Отправляю пакет данных
        //

        public void SendData(string answer) {

            //Проверяю жив ли сервер
            if (isServer()) {
                buffer.Clear(); //Подготавливаю место в памяти для отправки
                pkgLength = Encoding.Unicode.GetBytes(answer).Length + sizeof(int); //Вычисляю длину посылки

                //Формирую пакет посылки
                buffer.AddRange(BitConverter.GetBytes(pkgLength)); //Пишу в начало длину пакета
                buffer.AddRange(Encoding.Unicode.GetBytes(answer)); //Потом пишу сами данные
                socket.Send(buffer.ToArray()); //Отправляю серверу посылку
            }
        }

        //Закрываем сокет
        //

        public void Close() {

            if (socket != null) { //Если с сокетом всё норм и он не закрылся по ошибке, закрываю как цивилизованный человек
                socket.Shutdown(SocketShutdown.Both);
                socket.Close();
            }
        }




    }
}
