﻿// Файл обеспечивает сохранность данных сессии, чтобы не потерять принятое предложение
//

using Android.App;
using Android.Content;
using JosephStalinForTheProletarians.DataModels;
using Newtonsoft.Json;

namespace JosephStalinForTheProletarians.Helpers {
    public class Session {

        public string ServerIP { get; set; }
        public int ServerPort { get; set; }

        //Принятое предложение
        public FullOfferInfo AcceptedOffer {
            get {

                //Получаю сохранённое предложение
                return GetAcceptedOffer();
            }
        }

        //Рыночная ставка на момент принятия предложения
        public int MarketRateAccepted { get {

                //Получаю сохранённую рыночную ставку
                return GetAcceptedMarketRate();
            }
        }

        #region SAVE SESSION
        //Создаю файл в котором буду сохранять данные
        ISharedPreferences preferences = Application.Context.GetSharedPreferences("MySession", FileCreationMode.Private);
        ISharedPreferencesEditor editor; // Объект который будет записывать сохраняемые данные

        public Session() {
            editor = preferences.Edit(); //Связываю записывающий объект с файлом в который будет писать

            // Закрываю данные адреса и порта. Ставить свои.
            ServerIP ="185.104.113.166";
            ServerPort= 1325;
        }





        // Получаю из сохранения принятое предложение
        //

        private FullOfferInfo GetAcceptedOffer() {
            string json = preferences.GetString("Помогаю", null); //Беру данные из сохранения
            if (!string.IsNullOrEmpty(json)) { //Если эти данные есть
                return JsonConvert.DeserializeObject<FullOfferInfo>(json); // Расшифровываю и отдаю объект сессии
            }
            return null; //Если данных в сохранении нет, возвращаю null
        }

        // Получаю из сохранений принятую ставку рынка
        //

        private int GetAcceptedMarketRate() {
            return preferences.GetInt("Рыночная ставка", 0); //Возвращаю ставку из сохранения
        }

        // Сохраняю принятое приглашение
        //

        public void SaveAcceptedOffer(ref FullOfferInfo offer, int marketRate) {
            string json = JsonConvert.SerializeObject(offer); //Сериализую
            editor.PutString("Помогаю", json); //Пишу само приглашение
            editor.PutInt("Рыночная ставка", marketRate); //Пишу текущую ставку рынка
            editor.Apply(); //Утверждаю запись и записываю в файл
        }

        // Очищаю сохранение
        //

        public void ClearAcceptedOffer() {
            editor.PutString("Помогаю", null);
            editor.Apply();
        }


        // Получаю телефонный номер 
        //

        public string GetPhone() {
            return preferences.GetString("Номер", "");
        }

        // Сохраняю телефонный номер
        //

        public void SavePhone(string phone) {
            editor.PutString("Номер", phone);
            editor.Apply();
        }


        public void SavePin(string pin) {
            editor.PutString("Пин", pin);
            editor.Apply();
        }

        public string GetPin() {
            return preferences.GetString("Пин", "");
        }
        #endregion 
    }
}
