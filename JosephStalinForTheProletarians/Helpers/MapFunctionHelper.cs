﻿/// Помощник который непосредственно обрабатывает всё что связано с картографическими данными
using System;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using Android.Graphics;
using Java.Util;
using JosephStalinForTheProletarians.DataModels;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Essentials;
using Mapsui.Projection;

namespace JosephStalinForTheProletarians.Helpers {
    public class MapFunctionHelper {

        Mapsui.Map mMap;
        Location oLocation;
        Location dLocation;

        //Данные по маршруту
        DirectionResponse direction = new DirectionResponse();


        string json = string.Empty;
        private HttpClient hClient;

        public MapFunctionHelper(Mapsui.Map map, Location myLocation, Location destination) {
            oLocation = myLocation;
            dLocation = destination;
            mMap = map;

            hClient = new HttpClient();
        }

        public MapFunctionHelper(Location myLocation, Location destination) {
            oLocation = myLocation;
            dLocation = destination;

            hClient = new HttpClient();
        }


        //Отправляю запрос на построение маршрута и принимаю ответ
        private readonly string baseRouteUrl = "http://router.project-osrm.org/route/v1/driving/";

        public async Task<string> GetDirectionJsonAsync(Location origin, Location destination) {
            oLocation = origin;


            string sOrigin = origin.Longitude.ToString().Replace(",", ".") + "," + origin.Latitude.ToString().Replace(",", ".");
            string sDestination = destination.Longitude.ToString().Replace(",", ".") + "," + destination.Latitude.ToString().Replace(",", ".");

            // Собираю строку на запрос
            string url = string.Format(baseRouteUrl) +
                    $"{sOrigin}" +
                    $";{sDestination}" +
                    "?overview=full" +
                    "&geometries=polyline" +
                    "&steps=false";


            HttpResponseMessage response = await hClient.GetAsync(url); //Отправляю данные и получаю запрос
            if (response.IsSuccessStatusCode) {
                string json = await response.Content.ReadAsStringAsync(); //Считываю json-ответ
                direction = JsonConvert.DeserializeObject<DirectionResponse>(json); //Расшифровываю json в объект c#

                return json;

            } else {
                return null;
            }
        }



        // Отрисовываю маршрут на карте
        //

        List<Route> routes;
        List<Location> locations = new List<Location>();

        public bool DrawTripOnMap(Mapsui.Map map, string json) {

            return false;
            //////Беру объекты для отрисовки
            ////this.json = json;
            //mMap = map;
            //mMap.ClearCache();

            ////Если удалось получить маршрут
            //if (direction != null && direction.Code == "Ok") {

            //    // Беру массив точек на карте из данных маршрута
            //    routes = direction.Routes.ToList();

            //    locations = DecodePolylinePoints(routes[0].Geometry.ToString());

            //    Location oPinLocation = locations[0]; //Местоположение первой отметки
            //    Location dPinLocation = locations[locations.Count() - 1]; //Местоположение последней отметки

            //    MarkerOptions oMarker = new MarkerOptions();

            //    LatLng oPinLatLng = new LatLng(oPinLocation.Latitude, oPinLocation.Longitude);
            //    oMarker.SetPosition(oPinLatLng);
            //    oMarker.SetIcon(BitmapDescriptorFactory.DefaultMarker(BitmapDescriptorFactory.HueYellow));
            //    gMap.AddMarker(oMarker);

            //    MarkerOptions dMarker = new MarkerOptions();

            //    LatLng dPinLatLng = new LatLng(dPinLocation.Latitude, dPinLocation.Longitude);
            //    dMarker.SetPosition(dPinLatLng);
            //    dMarker.SetIcon(BitmapDescriptorFactory.DefaultMarker(BitmapDescriptorFactory.HueBlue));
            //    gMap.AddMarker(dMarker);


            //    //Создаю и заполняю массив точек для отрисовки
            //    ArrayList dots = new ArrayList();
            //    foreach (Location dot in locations) {

            //        //dots.Add(dot);
            //        dots.Add(new LatLng(dot.Latitude, dot.Longitude));
            //    }


            //    //Создаю объект который отрисует маршрут на карте
            //    PolylineOptions polylineOptions = new PolylineOptions()
            //        .AddAll(dots)  //Сюда пишу сразу все точки местоположений маршрута
            //        .InvokeWidth(20)
            //        .InvokeColor(Color.Teal)
            //        .InvokeStartCap(new SquareCap())
            //        .InvokeEndCap(new SquareCap())
            //        .InvokeJointType(JointType.Round)
            //        .Geodesic(true);

            //    gMap.AddPolyline(polylineOptions);

            //    //Устанавливаю обзор камеры на весь маршрут
            //    gMap.UiSettings.ZoomControlsEnabled = true;
            //    gMap.AnimateCamera(CameraUpdateFactory.NewLatLngZoom(dPinLatLng, 18));

            //    return true;
            //} else {
            //    return false;
            //}
        }


        private List<Location> DecodePolylinePoints(string encodedPoints) {
            if (encodedPoints == null || encodedPoints == "") return null;
            List<Location> poly = new List<Location>();
            char[] polylinechars = encodedPoints.ToCharArray();
            int index = 0;

            int currentLat = 0;
            int currentLng = 0;
            int next5bits;
            int sum;
            int shifter;


            while (index < polylinechars.Length) {
                // Считаем следующую широту
                sum = 0;
                shifter = 0;
                do {
                    next5bits = (int)polylinechars[index++] - 63;
                    sum |= (next5bits & 31) << shifter;
                    shifter += 5;
                } while (next5bits >= 32 && index < polylinechars.Length);

                if (index >= polylinechars.Length)
                    break;

                currentLat += (sum & 1) == 1 ? ~(sum >> 1) : (sum >> 1);

                //Считаем следующую долготу
                sum = 0;
                shifter = 0;
                do {
                    next5bits = (int)polylinechars[index++] - 63;
                    sum |= (next5bits & 31) << shifter;
                    shifter += 5;
                } while (next5bits >= 32 && index < polylinechars.Length);

                if (index >= polylinechars.Length && next5bits >= 32)
                    break;

                currentLng += (sum & 1) == 1 ? ~(sum >> 1) : (sum >> 1);
                double latitude = Convert.ToDouble(currentLat) / 100000.0;
                double longitude = Convert.ToDouble(currentLng) / 100000.0;
                Location p = new Location(latitude, longitude);
                poly.Add(p);
            }

            return poly;
        }

        // Получаю продолжительность движения
        //

        double Duration;
        public double GetTimeOfTripMins() {
            if (routes != null) {
                Duration = Math.Round((double)routes[0].Duration / 60, 0);
            } else {
                Duration = GetStandartTimeOfTripMins();
            }
            return Duration;
        }

        //Получаю стандартное время с расчётом 1км = 1,5 мин
        //

        double sTimeMins;
        public double GetStandartTimeOfTripMins() {

            
            sTimeMins = DistanceAlgorithm.DistanceBetweenPlaces(oLocation.Longitude,oLocation.Latitude, dLocation.Longitude, dLocation.Latitude) *1.5;
            return sTimeMins;
        }

        // Очищаю карту от всех меток
        //

        public void ClearMap() {
            mMap.Layers.Clear();
        }

        // Получаю полное время на проезд до точки и обратно
        //

        public double GetFullTimeMins(int labHours) {
            if (Duration == 0) {
                GetTimeOfTripMins();
            }

            //Calculate fulltrip include come back
            double fullTripDuration = Duration * 2;

            double labMins = labHours * 60; //minutes
            double fullTimeMins = labMins + fullTripDuration;

            return fullTimeMins; //minutes
        }

    }



    // Класс для того чтобы первести геометрию земного шара в километры
    //

    static class DistanceAlgorithm{
        const double PIx = 3.141592653589793;
        const double RADIUS = 6378.16;  //Радиус земного шара в км

        /// <summary>
        /// Convert degrees to Radians
        /// Первожу градусы в радианы.
        /// </summary>
        /// <param name="x">Degrees</param>
        /// <returns>The equivalent in radians</returns>
        public static double Radians(double x){
            return x * PIx / 180;
        }

        /// <summary>
        /// Calculate the distance between two places.
        /// Вычисляю дистанцию между местами.
        /// </summary>
        /// <param name="lon1"></param>
        /// <param name="lat1"></param>
        /// <param name="lon2"></param>
        /// <param name="lat2"></param>
        /// <returns></returns>
        public static double DistanceBetweenPlaces(double lon1, double lat1, double lon2, double lat2){
            double dlon = Radians(lon2 - lon1);
            double dlat = Radians(lat2 - lat1);

            double a = (Math.Sin(dlat /2) *Math.Sin(dlat /2)) +Math.Cos(Radians(lat1)) *Math.Cos(Radians(lat2)) *(Math.Sin(dlon /2) *Math.Sin(dlon /2));
            double angle = 2 *Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 -a));
            return angle * RADIUS;
        }

    }
}
