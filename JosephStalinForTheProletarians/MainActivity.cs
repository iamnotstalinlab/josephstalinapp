﻿
// Модуль является основой приложения, так как:
// - связывает фрагменты между собой и проводит обмен данными между ними
// - выполняет основные системные вызовы приложения

using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Widget;
using Android.Support.V4.View;
using System;
using JosephStalinForTheProletarians.Views;
using JosephStalinForTheProletarians.Fragments;
using Android.Graphics;
using Android.Content;
using JosephStalinForTheProletarians.DataModels;
using JosephStalinForTheProletarians.Adapters;
using JosephStalinForTheProletarians.Helpers;
using com.refractored;

namespace JosephStalinForTheProletarians {
    [Activity(Label = "@string/app_name", Theme = "@style/JosephStalinTheme", MainLauncher = false, NoHistory = true, ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]

    public class MainActivity : AppCompatActivity {



        #region CREATE ACTIVITY AND CONECT VIEWS

        FullOfferInfo offer;
        Session session;

        /// Создаю активити
        /// 

        protected override void OnCreate(Bundle savedInstanceState) {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);

            //Создаю текущий сеанс
            session = new Session();

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);
            ConnectViews();

        }

        //Views
        PagerSlidingTabStrip tabs;
        StaticViewPager staticPager;

        //Fragments
        MapsFragment mapsFragment = new MapsFragment();
        HelpFragment helpFragment = new HelpFragment();
        MarketFragment marketFragment = new MarketFragment();


        /// Связываю представления
        /// 

        void ConnectViews() {

            tabs = (PagerSlidingTabStrip)FindViewById(Resource.Id.tabs);

            staticPager = (StaticViewPager)FindViewById(Resource.Id.viewpager);
            staticPager.OffscreenPageLimit = 3;
            SetUpViewPager();

            tabs.SetViewPager(staticPager);
            tabs.SetTabTextColor(Color.LightGray);

            marketFragment.OpenInfo += MarketFragment_OpenInfo;
            marketFragment.ExceptionEvent += MarketFragment_ExceptionEvent;

            mapsFragment.OfferLoaded += MapsFragment_OfferLoaded;
            mapsFragment.NewUpdate += MapsFragment_NewUpdate; ;
            mapsFragment.OfferAccepted += MapsFragment_OfferAccepted;
            mapsFragment.CallCustomer += MapsFragment_CallCustomer;
            mapsFragment.Navigate += MapsFragment_Navigate;
            mapsFragment.InvitationEnded += MapsFragment_InvitationEnded;
            mapsFragment.ExceptionEvent += MapsFragment_ExceptionEvent;

            helpFragment.OpenSalary += HelpFragment_OpenSalary;
            helpFragment.OpenCommunity += HelpFragment_OpenCommunity;
            helpFragment.OpenSoviet += HelpFragment_OpenSoviet;
            helpFragment.OpenDice += HelpFragment_OpenDice;
        }



        /// Устанавливаю набор фрагментов
        /// 


        ViewPagerAdapter adapter;
        private void SetUpViewPager() {
            adapter = new ViewPagerAdapter(SupportFragmentManager);
            adapter.AddFragment(marketFragment, "Рынок");
            adapter.AddFragment(mapsFragment, "Карты");
            adapter.AddFragment(helpFragment, "Помощь");

            staticPager.Adapter = adapter;
            staticPager.IsLockedSwipe = true;
        }

        

        #endregion

        #region MAPS FRAGMENT EVENTS


        //Обновление загруженного предложения из сохранений
        private void MapsFragment_OfferLoaded(object sender, MapsFragment.FullInfoEventArgs e) {
            offer = e.FullInformation;
        }

        int mRate;
        bool isLoaded;
        //Обновление местоположения
        private void MapsFragment_NewUpdate(object sender, MapsFragment.LocationUpdatedEventArgs e) {

            marketFragment.Update(e.Location);

            /// Загружаю принятое приглашение если оно есть
            if (!isLoaded
                && session.AcceptedOffer != null) {

                isLoaded = true;
                offer = session.AcceptedOffer;
                mRate = session.MarketRateAccepted;

                mapsFragment.LoadOffer(offer, mRate);
            }
        }

        //Обрабатываю принятое приглашение
        private void MapsFragment_OfferAccepted(object sender, MapsFragment.FullInfoEventArgs e) {
            offer = e.FullInformation;
            marketFragment.OfferAccepted(offer);

            //Сохраняю принятое предложение чтобы не потерять данные
            session.SaveAcceptedOffer(ref offer, mRate);
        }

        //Открываю навигатор
        private void MapsFragment_Navigate(object sender, EventArgs e) {

            string uriString = "";

            //It is not documentation, but "mode=r"is public transit (Общественный транспорт)
            uriString = "google.navigation:q="
                + offer.PickUpLat.ToString().Replace(",", ".") + ","
                + offer.PickUpLng.ToString().Replace(",", ".") + "&mode=r";

            Android.Net.Uri googleMapIntentUri = Android.Net.Uri.Parse(uriString);
            Intent mapIntent = new Intent(Intent.ActionView, googleMapIntentUri);
            mapIntent.SetPackage("com.google.android.apps.maps");

            try {
                StartActivity(mapIntent);
            } catch {

                Toast.MakeText(this, "Google карты не установлены на этом устройстве.", ToastLength.Short).Show();
            }
        }

        //Открываю связь с заказчиком
        private void MapsFragment_CallCustomer(object sender, EventArgs e) {

            //Проверяю, если все цифры, значит телефон
            string sContact = offer.CustomerContact.Substring(1);
            if (long.TryParse(sContact, out long r)) {

                var uri = Android.Net.Uri.Parse("tel:" + offer.CustomerContact);
                Intent intent = new Intent(Intent.ActionDial, uri);
                StartActivity(intent);
            } else {

                //Иначе - внешний контакт
                string uriString = offer.CustomerContact;
                OpenExternalResource(ref uriString);
            }
        }

        //Обрабатываю завершённое приглашение
        private void MapsFragment_InvitationEnded(object sender, EventArgs e) {
            CollectPaymentFragment collectPaymentFragment = new CollectPaymentFragment(offer.Wage);

            collectPaymentFragment.Cancelable = false;
            var trans = SupportFragmentManager.BeginTransaction();
            collectPaymentFragment.Show(trans, "pay");

            collectPaymentFragment.PaymentCollected += (o, u) => {
                collectPaymentFragment.Dismiss();
            };

            //Удаляю принятое приглашение из сохранённых
            session.ClearAcceptedOffer();
        }


        /// Вывожу ошибку на экран
        ///

        private void MapsFragment_ExceptionEvent(object sender, MapsFragment.ExceptionEventArgs e) {
            string message = e.Message;
            Toast.MakeText(this, message, ToastLength.Short).Show();
        }

        #endregion

        #region MARKET FRAGMENT EVENTS

        //Открываю выбранное предложение в экране карт
        private void MarketFragment_OpenInfo(object sender, MarketFragment.FullInfoEventArgs e) {

            //Если нет уже выполняемого приглашения
            if (session.AcceptedOffer == null) {
                offer = e.FullInformation;
                mRate = e.MarketRate;

                //Вызываю метод в картах, который отображает сведения по предложению
                mapsFragment.OpenInvitation(offer, mRate);
                staticPager.SetCurrentItem(1, true);
            } else {
                Toast.MakeText(this, "Сначала следует завершить действующее предложение.", ToastLength.Short).Show();
                staticPager.SetCurrentItem(1,true);
            }
        }

        //Вывожу ошибку на экран
        private void MarketFragment_ExceptionEvent(object sender, MarketFragment.ExceptionEventArgs e) {
            string message = e.Message;
            RunOnUiThread(() => {
                Toast.MakeText(this, message, ToastLength.Short).Show();
            });
        }
        #endregion

        #region HELP FRAGMENT EVENTS

        //Открываю экран с жалованием
        private void HelpFragment_OpenSalary(object sender, EventArgs e) {
            string uriString = "https://iamnotstalin.ru/titry/";
            OpenExternalResource(ref uriString);
        }

        //Открываю чат сообщества
        private void HelpFragment_OpenCommunity(object sender, EventArgs e) {
            string uriString = "https://t.me/JosephStalinCommunity";
            OpenExternalResource(ref uriString);
        }

        //Открываю чат совета
        private void HelpFragment_OpenSoviet(object sender, EventArgs e) {
            string uriString = "https://t.me/joinchat/LfUiulcM3wtb6UbVQrBBEg";
            OpenExternalResource(ref uriString);
        }

        //Открываю чат с игровым турниром
        private void HelpFragment_OpenDice(object sender, EventArgs e) {
            string uriString = "https://t.me/iamnotstalingames";
            OpenExternalResource(ref uriString);
        }
        #endregion

        #region COMMON ACTIONS
        /// Открываю внешние ресурсы в других приложениях
        ///

        void OpenExternalResource(ref string uriString) {
            Android.Net.Uri exIntentUri = Android.Net.Uri.Parse(uriString);
            Intent exIntent = new Intent(Intent.ActionView, exIntentUri);
            StartActivity(exIntent);
        }


        protected override void OnPause() {
            base.OnPause();

            //При паузе выключаю приложение, потому что фрагмент карт
            //почему-то теряет данные. Долго искал из-за чего и не нашёл.
            //Разбираться буду, походу ещё 40 лет, поэтому просто вырубаю приложение.

            Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
        }

        #endregion




    }
}