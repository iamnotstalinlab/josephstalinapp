﻿// Это представление обеспечивает отображение фрагментов с возможностью отключить их прокручиваение касаниями

using System;
using Android.Support.V4.View;
using Android.Views;
using Android.Content;

namespace JosephStalinForTheProletarians.Views {


    public class StaticViewPager : ViewPager {


        public bool IsLockedSwipe { get; set; } // Свойство для включения и отключения касаний

        Context mContext;

        public StaticViewPager(Context context) : base(context) { }

        public StaticViewPager(Context context, Android.Util.IAttributeSet attrs) : base(context, attrs) {
            Init(context, attrs);
        }

        private void Init(Context context, Android.Util.IAttributeSet attrs) {
            mContext = context;
        }

        //Обрабатываю касания и если они отключены, то даю отрицательный ответ
        //

        public override bool OnTouchEvent(MotionEvent e) {
            return !IsLockedSwipe && base.OnTouchEvent(e);
        }

        //Обрабатываю касания и если они отключены, то даю отрицательный ответ
        //

        public override bool OnInterceptTouchEvent(MotionEvent ev) {
            return !IsLockedSwipe && base.OnInterceptTouchEvent(ev);
        }

        //Обрабатываю касания и если они отключены, то даю отрицательный ответ
        //

        public override bool CanScrollHorizontally(int direction) {
            return !IsLockedSwipe && base.CanScrollHorizontally(direction);
        }
    }
}
