﻿// Фрагмент показывает сколько денег помощник получил. Никаких других функций не выполняет
//

using System;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace JosephStalinForTheProletarians.Fragments {
    public class CollectPaymentFragment : Android.Support.V4.App.DialogFragment {

        int mWage;

        TextView wageText;
        TextView moneyText;

        Button collectPayButton;

        public event EventHandler PaymentCollected;

        public CollectPaymentFragment(int wage) {
            mWage = wage;
        }

        public override void OnCreate(Bundle savedInstanceState) {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        //Создаю и связываю представления
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            // Use this to return your custom view for this Fragment
            View view =inflater.Inflate(Resource.Layout.CollectPayment, container, false);

            wageText = (TextView)view.FindViewById(Resource.Id.wageText);
            moneyText = (TextView)view.FindViewById(Resource.Id.moneyText);

            collectPayButton = (Button)view.FindViewById(Resource.Id.collectPayButton);

            wageText.Text = mWage.ToString();
            moneyText.Text = GetMoneyText();

            collectPayButton.Click += CollectPayButton_Click;

            return view;
        }

        // Уведомляю о нажатии кнопки
        //

        private void CollectPayButton_Click(object sender, EventArgs e) {
            PaymentCollected?.Invoke(this, new EventArgs());
        }

        // Пишу сопровождающий текст грамотно
        //

        public string GetMoneyText() {
            string text;

            int moneyStatus = Math.Abs(mWage % 10);
            int absStatus = Math.Abs(mWage);

            if ((moneyStatus > 1) && (moneyStatus < 5) && !((absStatus > 10) && (absStatus < 20)) && (absStatus <99)) {
                text = " рубля";
            } else {
                if ((moneyStatus == 1) && (absStatus != 11) && (absStatus <99)) {
                    text = " рубль";
                } else {
                    text = " рублей";
                }
            }

            return text;
        }


    }
}
