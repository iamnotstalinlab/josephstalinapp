﻿namespace JosephStalinForTheProletarians.DataModels
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    //public class LatLng {
    //    public double Latitude { get; set; }
    //    public double Longitude { get; set; }

        //public LatLng(double latitude, double longitude) {
        //    Latitude = latitude;
        //    Longitude = longitude;
        //}
    //}

    public partial class DirectionResponse
    {
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("waypoints")]
        public List<Waypoint> Waypoints { get; set; }

        [JsonProperty("routes")]
        public List<Route> Routes { get; set; }
    }

    public partial class Route
    {
        [JsonProperty("geometry")]
        public string Geometry { get; set; }

        [JsonProperty("legs")]
        public List<Leg> Legs { get; set; }

        [JsonProperty("weight_name")]
        public string WeightName { get; set; }

        [JsonProperty("weight")]
        public double Weight { get; set; }

        [JsonProperty("distance")]
        public double Distance { get; set; }

        [JsonProperty("duration")]
        public double Duration { get; set; }
    }

    public partial class Leg
    {
        [JsonProperty("steps")]
        public List<object> Steps { get; set; }

        [JsonProperty("weight")]
        public double Weight { get; set; }

        [JsonProperty("distance")]
        public double Distance { get; set; }

        [JsonProperty("summary")]
        public string Summary { get; set; }

        [JsonProperty("duration")]
        public double Duration { get; set; }
    }

    public partial class Waypoint
    {
        [JsonProperty("hint")]
        public string Hint { get; set; }

        [JsonProperty("location")]
        public List<double> Location { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
