﻿
//// Фрагмент отображает свободные предложения
//// и производит все обмены данных с сервером
////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Android.App;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using JosephStalinForTheProletarians.Adapter;
using JosephStalinForTheProletarians.DataModels;
using JosephStalinForTheProletarians.EventListeners;
using Mapsui.Projection;
using Point = Mapsui.Geometries.Point;
using Newtonsoft.Json;
using Xamarin.Essentials;
using System.Globalization;
using JosephStalinForTheProletarians.Helpers;

namespace JosephStalinForTheProletarians.Fragments {
    public class MarketFragment : Android.Support.V4.App.Fragment {

        #region CREATE VIEW
        /// Обрабатываю при создании представления на экземпляре
        ///

        //Активити сохраняю в переменную, чтобы при паузе приложения не потерять её
        //и не схватить ошибку.
        //Обрабатываю представление при создании экземпляра представления

        Activity activity;
        Session session;

        public override void OnCreate(Bundle savedInstanceState) {
            base.OnCreate(savedInstanceState);

            activity = this.Activity;
            session =new Session();
        }

        //Загружаю интерфейс из макета в представление
        ///

        View view;
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            // Use this to return your custom view for this Fragment

            view = inflater.Inflate(Resource.Layout.market, container, false);

            

            //Связываю элементы с экземплярами обработчиков
            ConnectViews();
            Update(null);
            return view;
        }


        
        
        /// Связываю элементы с экземплярами обработчиков 
        ///
       
        int mMarketRate;

        MarketAdapter adapter;
        List<FullOfferInfo> offers = new List<FullOfferInfo>();

        RecyclerView recyclerView;
        Android.Support.V7.Widget.LinearLayoutManager layoutManager;
        void ConnectViews() {

            //Создаю представление для списка предложений
            recyclerView = (RecyclerView)view.FindViewById(Resource.Id.recyclerView);

            layoutManager = new Android.Support.V7.Widget.LinearLayoutManager(recyclerView.Context);
            recyclerView.SetLayoutManager(layoutManager);

            //Создаю список отображаемых представлений
            adapter = new MarketAdapter(ref offers, ref mMarketRate);
            adapter.ItemButtonClick += MarketAdapter_ItemNextStepClick;

            //Устанавливаю отображаемый список в представление списка
            recyclerView.SetAdapter(adapter);
        }

        #endregion

        #region MAKE UDPATE AND GET OFFERS FROM SERVER

        //// Обновляю список приглашений
        ///

        Location myLocation;
        public void Update(Location location) {
            if(location != null) {
                myLocation = location;
            }

            Thread thread = new Thread(new ThreadStart(GetOffers));
            thread.Start();

            UpdateOffers();
        }

        //// Запрашиваю и получаю приглашения от сервера
        ///


       
        private void GetOffers() {
            TcpConnector connector = new TcpConnector(session.ServerIP, session.ServerPort);

            connector.Connect();
            Request request = new Request(session.GetPhone(),"Помощник", "Выполнить", 0, null);
            string json = JsonConvert.SerializeObject(request, Formatting.Indented);
            connector.SendData(json);

            json = connector.GetData();
            connector.SendData("Принял");
            connector.Close();

            request = JsonConvert.DeserializeObject<Request>(json);

            List<FullOfferInfo> nOffers = JsonConvert.DeserializeObject<List<FullOfferInfo>>(request.Content);
            ParsingData(ref nOffers);
            offers = nOffers.OrderByDescending(o => o.Timestamp).ToList();

            mMarketRate = request.MarketRate;
        }

        /// Вычисляю радиус по прямой до каждого места работы
        /// 

        Point myPoint;  //Моя точка на карте, для расчёта радиуса

        private void ParsingData(ref List<FullOfferInfo> nOffers) {
            if(myLocation != null) {

                Location offLocation =new Location();
                double radius =0;
                foreach(FullOfferInfo off in nOffers) {

                    offLocation.Latitude =off.PickUpLat;
                    offLocation.Longitude =off.PickUpLng;
                    
                    //Получаю расстояние по прямой до точки в километрах.
                    radius = DistanceAlgorithm.DistanceBetweenPlaces(myLocation.Longitude, myLocation.Latitude,
                        offLocation.Longitude, offLocation.Latitude);
                    off.Radius = Math.Round((double)radius, 2); //Округляю дистаницю
                }
            }
        }


        //// Обновляю список приглашений на экране
        ////

        private void UpdateOffers() {
            adapter.Items =offers;
            adapter.MarketRate = mMarketRate;

            activity.RunOnUiThread(() => {
                adapter?.NotifyDataSetChanged();
            });
        }

        #endregion

        #region SELECT OFFER
        /// Обрабатываю нажатие на кнопку со стрелочкой на приглашении из списка
        ///

        int aIndex;     //aIndex - индекс активного приглашение

        public event EventHandler<FullInfoEventArgs> OpenInfo; // Событие уведмоляет фрагмент "Карты", что следуте показать данные
        public class FullInfoEventArgs : EventArgs {
            public FullOfferInfo FullInformation { get; set; }
            public int MarketRate { get; set; }
        }


        private void MarketAdapter_ItemNextStepClick(object sender, MarketAdapter.MarketAdapterClickEventArgs e) {
            aIndex = e.Position;
            if(offers.Count > aIndex) {
                OpenInfo?.Invoke(this, new FullInfoEventArgs { FullInformation = offers[aIndex], MarketRate =mMarketRate });
            } else {
                ExceptionEvent?.Invoke(this, new ExceptionEventArgs { Message = "На предложение уже отозвался другой работник." });
            }
        }

        #endregion

        #region ACCEPT OFFER
        /// Подготавливаю данные для оправки на сервер уведомления о принятии и удаления из списка предложений
        ///

        public event EventHandler<ExceptionEventArgs> ExceptionEvent; //Событие сообщает что произошла ошибка
        public class ExceptionEventArgs : EventArgs {
            public string Message { get; set; }
        }

        public void OfferAccepted(FullOfferInfo offer) {
            Thread thread = new Thread(new ParameterizedThreadStart(AcceptOffer));
            thread.Start(offer);
        }



        /// Сообщаю серверу, что приглашение приняли и удаляю его из списка на экране
        ///

        private void AcceptOffer(object tOffer) {
            FullOfferInfo offer = (FullOfferInfo) tOffer;
            TcpConnector connector = new TcpConnector(session.ServerIP, session.ServerPort);

            connector.Connect();
            string content = JsonConvert.SerializeObject(offer);
            Request request = new Request(session.GetPhone(), "Помощник", "Завершить", 0, content);

            string json = JsonConvert.SerializeObject(request);
            connector.SendData(json);
            connector.Close();

            if (offers.Contains(offer)) {
                offers.RemoveAt(aIndex);
            }
            Update(myLocation);
        }

        #endregion
    }
}
