﻿
//// Фрагмент отображает всю официальну дополнительную информацию,
//// включая ссылки для связи.

using System;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace JosephStalinForTheProletarians.Fragments {
    public class HelpFragment : Android.Support.V4.App.Fragment {

        #region CREATE AND CONNECT VIEWS

        public override void OnCreate(Bundle savedInstanceState) {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        /// Заполняю макет
        /// 

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            // Use this to return your custom view for this Fragment
            View view =inflater.Inflate(Resource.Layout.help, container, false);

            ConnectViews(ref view);
			return view;
        }


        /// Связываю представления макета
        /// 
        
        ImageButton communityButton;
        ImageButton sovietButton;
        ImageButton diceButton;

        Button salaryButton;

        public event EventHandler OpenSalary;
        public event EventHandler OpenCommunity;
        public event EventHandler OpenCourt;
        public event EventHandler OpenSoviet;
        public event EventHandler OpenDice;

        void ConnectViews(ref View view) {
            salaryButton = (Button)view.FindViewById(Resource.Id.salaryButton);

            communityButton = (ImageButton)view.FindViewById(Resource.Id.communityButton);
            sovietButton = (ImageButton)view.FindViewById(Resource.Id.sovietButton);
            diceButton = (ImageButton)view.FindViewById(Resource.Id.diceButton);

            salaryButton.Click += (o, e) => {
                OpenSalary?.Invoke(this, new EventArgs());
            };

            sovietButton.Click += (o, e) => {
                OpenSoviet?.Invoke(this, new EventArgs());
            };
            communityButton.Click += (o, e) => {
                OpenCommunity?.Invoke(this, new EventArgs());    
            };
            diceButton.Click += (o, e) => {
                OpenDice?.Invoke(this, new EventArgs());
            };
        }
        #endregion

    }
}
