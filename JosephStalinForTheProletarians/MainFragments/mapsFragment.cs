﻿//// Фрагмент отображает все данные по выбранному приглашению 
//// и производит обработку всех данных внутри предложения
////

using System;
using System.Globalization;
using Android;
using Android.App;
using Android.Graphics;
using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using JosephStalinForTheProletarians.DataModels;
using JosephStalinForTheProletarians.Helpers;
using Mapsui;
using Mapsui.Projection;
using Mapsui.UI.Android;
using Mapsui.Utilities;
using Xamarin.Essentials;

namespace JosephStalinForTheProletarians.Fragments {
    public class MapsFragment : Android.Support.V4.App.Fragment{

        #region CREATE VIEW AND CONNECTING THEM
        //Активити сохраняю в переменную, чтобы при паузе и возврате не потерять её
        //и не схватить ошибку.
        Activity activity;

        public override void OnCreate(Bundle savedInstanceState) {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
            activity = this.Activity;
            
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            // Use this to return your custom view for this Fragment
            View view = inflater.Inflate(Resource.Layout.maps, container, false);
            ConnectViews(ref view);
            CreateMap(ref view);
            return view;
        }


        /// Связываю макет и представления
        ///

        //Layout
        LinearLayout invitationInfoLayout;
        LinearLayout fullInfoLayout;
        LinearLayout statusLayout;

        //ImageView
        ImageView marker;
        ImageView imageView;

        ImageView documentsIcon;
        ImageView specViewIcon;
        ImageView specDocumentsIcon;
        ImageView photoIcon;

        //TextvView
        TextView statusText;
        TextView statusWageText;
        TextView moneyText;

        TextView customerNameText;
        TextView dateText;

        TextView promoWageText;
        TextView promoMoneyText;
        TextView fromTimeText;
        TextView toTimeText;
        TextView commentText;
        TextView addressText;


        //Button
        ImageButton cancelWorkButton;
        ImageButton callCustomerButton;
        ImageButton navigateButton;

        Button actionButton;
        Button acceptButton;

        private void ConnectViews(ref View view) {

            marker = (ImageView)view.FindViewById(Resource.Id.marker);

            //Layouts
            invitationInfoLayout = (LinearLayout)view.FindViewById(Resource.Id.invitationInfoLayout);
            fullInfoLayout = (LinearLayout)view.FindViewById(Resource.Id.fullInfoLayout);
            statusLayout = (LinearLayout)view.FindViewById(Resource.Id.statusLayout);

            //ImageViews
            imageView = (ImageView)view.FindViewById(Resource.Id.imageView);
            documentsIcon = (ImageView)view.FindViewById(Resource.Id.documentsIcon);
            specViewIcon = (ImageView)view.FindViewById(Resource.Id.specViewIcon);
            specDocumentsIcon = (ImageView)view.FindViewById(Resource.Id.specDocumentsIcon);

            //TextViews
            statusText = (TextView)view.FindViewById(Resource.Id.statusText);
            statusWageText = (TextView)view.FindViewById(Resource.Id.statusWageText);
            moneyText = (TextView)view.FindViewById(Resource.Id.moneyText);

            customerNameText = (TextView)view.FindViewById(Resource.Id.customerNameText);
            dateText = (TextView)view.FindViewById(Resource.Id.dateText);

            promoWageText = (TextView)view.FindViewById(Resource.Id.promoWageText);
            promoMoneyText = (TextView)view.FindViewById(Resource.Id.promoMoneyText);
            fromTimeText = (TextView)view.FindViewById(Resource.Id.fromTimeText);
            toTimeText = (TextView)view.FindViewById(Resource.Id.toTimeText);
            commentText = (TextView)view.FindViewById(Resource.Id.commentText);
            addressText = (TextView)view.FindViewById(Resource.Id.addressText);


            //Buttons
            cancelWorkButton = (ImageButton)view.FindViewById(Resource.Id.cancelWorkButton);
            callCustomerButton = (ImageButton)view.FindViewById(Resource.Id.callCustomerButton);
            navigateButton = (ImageButton)view.FindViewById(Resource.Id.navigateButton);
            actionButton = (Button)view.FindViewById(Resource.Id.actionButton);
            acceptButton = (Button)view.FindViewById(Resource.Id.acceptButton);


            cancelWorkButton.Click += CancelWorkButton_Click;
            callCustomerButton.Click += CallCustomerButton_Click;
            navigateButton.Click += NavigateButton_Click;
            actionButton.Click += ActionButton_Click;
            acceptButton.Click += AcceptButton_Click;
        }

        

        


        #endregion

        #region UPDATE MAP AND LOAD ACCEPTED OFFER
        /// Обновляю отображение карты и делаю загрузку сохранённого принятого предложения
        /// Убрал все карты. Они не работают должным образом (Гугл - не на всех устройствах, МапсЮАй - оставляет чёный след на экране. Это противно)
        /// Мы не гугл, у нас нет пока таких ресурсов чтобы использовать карты. А открытый код не готов их предоставить на сегодня.


        //MapControl mapControl;
        //Mapsui.Map map;

        FullOfferInfo offer;
        //public event EventHandler<FullInfoEventArgs> OfferLoaded;
        //public class FullInfoEventArgs : EventArgs {
        //    public FullOfferInfo FullInformation { get; set; }
        //}

        System.Timers.Timer uTimer;
        const int UPDATE_TIME = 10000;

        private void CreateMap(ref View view) {
            //mapControl = (MapControl)view.FindViewById(Resource.Id.mapControl);

            //map =new Mapsui.Map();
            //map.Layers.Add(OpenStreetMap.CreateTileLayer());
            //map.RotationLock =true;
            //map.PanLock = true;

            //mapControl.Map = map;

            if (CheckSpecialPermission()) {

                GetCurrentLocation();

                uTimer = new System.Timers.Timer(UPDATE_TIME);
                uTimer.Elapsed += Timer_Elapsed;
                uTimer.Start();
            }

        }

        /// Проверяю есть ли разрешения по доступу к геолокации
        ///

        const int RequestID = 0;
        readonly string[] permissionGroup = {
            Manifest.Permission.AccessCoarseLocation,
            Manifest.Permission.AccessFineLocation,
        };

        bool CheckSpecialPermission() {

            bool permissionGranted = false;
            if (ActivityCompat.CheckSelfPermission(activity, Manifest.Permission.AccessFineLocation) != Android.Content.PM.Permission.Granted
                && ActivityCompat.CheckSelfPermission(activity, Manifest.Permission.AccessCoarseLocation) != Android.Content.PM.Permission.Granted) {

                RequestPermissions(permissionGroup, RequestID);
            } else {
                permissionGranted = true;
            }
            return permissionGranted;
        }


        /// Выполняю запрос текущей локации по сигналу таймера
        /// 
        

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e) {
            GetCurrentLocation();
        }

        

        /// Получаю текущее местоположение
        ///

        Location mLastLocation;

        public event EventHandler<LocationUpdatedEventArgs> NewUpdate;
        public class LocationUpdatedEventArgs : EventArgs {
            public Location Location { get; set; }
        }

        private async void GetCurrentLocation() {

            //Update our LastLocation on the Map
            Location myLocation = await Geolocation.GetLastKnownLocationAsync();
            if (myLocation != null) {

                //Сделал условие ниже чтобы чутка сэкономить память и использовать глобальную переменную
                if (mLastLocation == null) {
                    mLastLocation = new Location(myLocation.Latitude, myLocation.Longitude);
                }

                mLastLocation.Latitude = myLocation.Latitude;
                mLastLocation.Longitude = myLocation.Longitude;
                NewUpdate?.Invoke(this, new LocationUpdatedEventArgs { Location = mLastLocation });

            } else {
                NewUpdate?.Invoke(this, new LocationUpdatedEventArgs { Location = null });
            }
        }


        //Убрал все карты. Не готов ещё ОпенСоурс к тому чтобы дать нам этот ресурс.

        //const int zoom = 17; //Уровень приближения камеры к карте
        //Mapsui.Geometries.Point tPoint =new Mapsui.Geometries.Point();

        //public void UpdateCamera(Location location) {
        //    map.PanLock =false; //Открываю перемещение карты, для смены местоположения

        //    tPoint = SphericalMercator.FromLonLat(location.Longitude, location.Latitude);

        //    //Переношу точку обзора
        //    mapControl.Navigator.NavigateTo(tPoint, map.Resolutions[zoom]);

        //    map.PanLock =true; //Закрываю возможность движения карты, после смены местоположения
        //}
        #endregion





        #region OPEN INVITATION
        /// Открываю предложение и провожу всю обработку его сведений
        /// 

        /// - inOffer Входящее предложение для отображения
        /// - marketRate Среднерыночная ставка

        MapFunctionHelper mapHelper;
        int mRate;

        public event EventHandler<ExceptionEventArgs> ExceptionEvent; //Событие сообщает что произошла ошибка
        public class ExceptionEventArgs : EventArgs {
            public string Message { get; set; }
        }

        Location dLocation;
        public void OpenInvitation(FullOfferInfo inOffer, int marketRate) {

            //Открываю только если знаю где находится
            if (mLastLocation != null) {

                offer = inOffer;
                mRate = marketRate;

                //Запускаю в главном потоке так как тут отрисовывается интерфейс
                activity.RunOnUiThread(() => {
                    ShowProgressDialogue();
                });

                //Никакой маршрут не строю, потому что сейчас у нас нет ресурсов на эти расчёты
                dLocation = new Location(offer.PickUpLat, offer.PickUpLng);
                mapHelper = new MapFunctionHelper(mLastLocation, dLocation);
                
                //LatLng pickUpLatLng = new LatLng(offer.PickUpLat, offer.PickUpLng);
                //string json = await mapHelper.GetDirectionJsonAsync(mLastLocation, pickUpLatLng);

                //UpdateCamera(dLocation);

                //Запускаю в главном потоке так как тут отрисовывается интерфейс
                activity.RunOnUiThread(() => {

                    ////Отрисовываю маршрут на карте
                    //if(!mapHelper.DrawTripOnMap(mainMap, json)) {
                    //    ExceptionEvent?.Invoke(this, new ExceptionEventArgs { Message = "Не удалось построить маршрут" });
                    //}

                    

                    //Собираю указанное в приглашении трудовое время
                    DateTime toTime = DateTime.Parse(offer.ToTime);
                    DateTime fromTime = DateTime.Parse(offer.FromTime);


                    //Вычисляю время труда
                    int labHours = (toTime.Hour - fromTime.Hour) + ((toTime.Minute - fromTime.Minute) / 60);
                    offer.FullTimeMins = mapHelper.GetFullTimeMins(labHours);

                    CreateInvitation();
                    CloseProgressDialogue();
                });
            } else {

                ExceptionEvent?.Invoke(this, new ExceptionEventArgs { Message = "Не получилось установить местоположение устройства" });
            }

        }

        /// Заполняю карточку предложения
        ///

        int mWage;
        int wage;

        void CreateInvitation() {

            //Открываю сразу все данные о предложении
            invitationInfoLayout.Visibility = ViewStates.Visible;
            fullInfoLayout.Visibility = ViewStates.Visible;
            actionButton.Text = "Скрыть";


            //Закрыл так как карты не использую и уменьшение здесь не уместно

            //marker.Visibility = ViewStates.Invisible;

            ////Настраиваю кнопку действия
            //actionButton.Text = "Подробнее";
            //actionButton.SetTextColor(Color.LightBlue);


            customerNameText.Text = offer.CustomerName;
            dateText.Text = GetTranslateDate();

            promoWageText.Text = offer.Wage.ToString();
            fromTimeText.Text = offer.FromTime;
            toTimeText.Text = offer.ToTime;

            commentText.Text = offer.Comment;
            addressText.Text = offer.PickUpAddress;

            if (offer.Documents == true) {
                documentsIcon.SetColorFilter(Color.LightGreen);
            }
            if (offer.SpecificDocuments == true) {
                specDocumentsIcon.SetColorFilter(Color.LightGreen);
            }
            if (offer.SpecificView == true) {
                specViewIcon.SetColorFilter(Color.LightGreen);
            }

            wage = offer.Wage;
            mWage = CalculateEarningWage();
            SettingUpDataInViews();


            
        }

        /// Перевожу дату чисел в строковой формат
        /// 

        private string GetTranslateDate() {

            //Андроид мудак и не понимает, что я хочу дату через слеш, поэтому приходится объяснять этому барану и переделывать всё самому 
            //А потом до разработчиков допрёт, что они наделали хуйни и я получу какую-нибудь ошибку) Ахуенно бля.

            DateTime sToday = DateTime.Today;
            DateTime sTomorrow = DateTime.Today.AddDays(1);
            DateTime sAfTomorrow = DateTime.Today.AddDays(2);

            DateTime dRequest = DateTime.Parse(offer.Date, CultureInfo.InvariantCulture);

            string retDate = "Сегодня";
            if (dRequest == sToday) {
                retDate = "Сегодня";
            }

            if (dRequest == sTomorrow) {
                retDate = "Завтра";
            }

            if (dRequest == sAfTomorrow) {
                retDate = "Послезавтра";
            }

            return retDate;
        }


        /// Считаю ожидаемую зарплату по приглашению на основе рыночной ставки
        /// 

        private int CalculateEarningWage() {

            //Собираю указанное в приглашении трудовое время
            DateTime toTime = DateTime.Parse(offer.ToTime);
            DateTime fromTime = DateTime.Parse(offer.FromTime);

            //Вычисляю время труда в часах
            double labHours = (toTime - fromTime).TotalMinutes /60;

            //Считаю заработок, который положен сейчас на рынке
            double eWage = mRate * labHours;

            //Округляю
            eWage = Math.Floor((eWage / 10) * 10);
            return Convert.ToInt32(eWage);
        }

        /// Считаю статус и вывожу его на поле в карточке
        /// 
        
        private void SettingUpDataInViews() {

            int sWage = wage - mWage;
            int absStatus = Math.Abs(sWage);

            if (sWage >= 0) {

                statusText.Text = "выше";
                statusText.SetTextColor(Color.Green);

                if (absStatus > 99) {
                    statusWageText.Text = "99+";
                    statusWageText.SetTextColor(Color.Green);
                } else {
                    statusWageText.Text = absStatus.ToString();
                    statusWageText.SetTextColor(Color.Green);
                }
            } else {
                if (sWage < 0) {
                    statusText.Text = "ниже";
                    statusText.SetTextColor(Color.OrangeRed);

                    if (absStatus > 99) {
                        statusWageText.Text = "99+";
                        statusWageText.SetTextColor(Color.OrangeRed);
                    } else {
                        statusWageText.Text = absStatus.ToString();
                        statusWageText.SetTextColor(Color.OrangeRed);

                    }
                }
            }
            int moneyStatus = Math.Abs(sWage % 10);

            if ((moneyStatus > 1) && (moneyStatus < 5) && !((absStatus > 10) && (absStatus < 20)) && (absStatus < 99)) {
                moneyText.Text = " рубля";
            } else {
                if ((moneyStatus == 1) && (absStatus != 11) && (absStatus < 99)) {
                    moneyText.Text = " рубль";
                } else {
                    moneyText.Text = " рублей";
                }
            }
        }

        #region BUTTONS
        /// Обработка кнопок
        /// 

        #region MAIN BUTTONS
        //Обработка основных кнопок: инормация и действие (принять/завершить)
        ///


        /// Кнопка которая сворачивает и разворачивает предложение
        /// 

        private void ActionButton_Click(object sender, EventArgs e) {

            if (fullInfoLayout.Visibility == ViewStates.Gone) {

                fullInfoLayout.Visibility = ViewStates.Visible;
                actionButton.Text = "Скрыть";
                actionButton.SetTextColor(Color.LightGray);
            } else {

                fullInfoLayout.Visibility = ViewStates.Gone;
                actionButton.Text = "Подробнее";
                actionButton.SetTextColor(Color.LightBlue);
            }
        }

        /// Принимаю предложение и завершаю его по этой кнопке
        ///

        
        public event EventHandler InvitationEnded;

        private void AcceptButton_Click(object sender, EventArgs e) {
            if (acceptButton.Text == "Принять") {
                AcceptInvitation();
            } else {
                ResetAfterInvitation();
                InvitationEnded?.Invoke(this, new EventArgs());
            }
        }

        /// Здесь вызываю диалог для подтверждения принятия предложения
        /// 

        public event EventHandler<FullInfoEventArgs> OfferAccepted;

        public event EventHandler<FullInfoEventArgs> OfferLoaded;
        public class FullInfoEventArgs : EventArgs {
            public FullOfferInfo FullInformation { get; set; }
        }


        private void AcceptInvitation() {

            Android.Support.V7.App.AlertDialog.Builder acceptInvitationAlert = new Android.Support.V7.App.AlertDialog.Builder(activity);
            acceptInvitationAlert.SetTitle("Готов помочь?");
            string message = "Мы уберём предложение из списка свободных.";

            acceptInvitationAlert.SetMessage(message);
            acceptInvitationAlert.SetPositiveButton("Готов!", (senderAlert, args) => {

                //Завершаю принятие предложения
                RunOffer();
                OfferAccepted?.Invoke(this, new FullInfoEventArgs { FullInformation = offer });
            });
            acceptInvitationAlert.SetNegativeButton("Передумал.", (senderAlert, args) => {
                acceptInvitationAlert.Dispose();
            });

            acceptInvitationAlert.Show();
        }

        /// Привожу карточку в порядок принятой к исполнению
        ///

        public void RunOffer() {

            // Запускаю в основном потоке, так как тут отрисовывается экран
            // и у этого метода есть публичный доступ

            activity.RunOnUiThread(() => {
                navigateButton.Visibility = ViewStates.Visible;
                if (!string.IsNullOrEmpty(offer.CustomerContact)) {
                    callCustomerButton.Visibility = ViewStates.Visible;
                }

                acceptButton.Text = "Закончить";
            });
        }

        //Загружаю предложение которое сейчас на исполнении
        //

        public void LoadOffer(FullOfferInfo inOffer, int marketRate) {

            offer = inOffer;
            mRate = marketRate;

            activity.RunOnUiThread(() => {
                CreateInvitation();
            });
            RunOffer();
        }

        //// Сбрасываю экран фрагмента до начального вида
        ///

        void ResetAfterInvitation() {

            acceptButton.Text = "Принять";
            invitationInfoLayout.Visibility = ViewStates.Gone;

            fullInfoLayout.Visibility = ViewStates.Gone;
            marker.Visibility = ViewStates.Visible;

            navigateButton.Visibility = ViewStates.Gone;
            callCustomerButton.Visibility = ViewStates.Gone;
        }


        #endregion

        #region TOP BUTTONS
        ///Обработка верних кнопок: отмена, навигация и связь
        ///


        public event EventHandler CancelInvitation;
        private void CancelWorkButton_Click(object sender, EventArgs e) {

            invitationInfoLayout.Visibility = ViewStates.Gone;

            fullInfoLayout.Visibility = ViewStates.Gone;
            marker.Visibility = ViewStates.Visible;
            
            mapHelper.ClearMap();

            CancelInvitation?.Invoke(this, new EventArgs());
        }


        public event EventHandler Navigate;
        private void NavigateButton_Click(object sender, EventArgs e) {
            Navigate.Invoke(this, new EventArgs());
        }


        public event EventHandler CallCustomer;
        private void CallCustomerButton_Click(object sender, EventArgs e) {
            CallCustomer.Invoke(this, new EventArgs());
        }
        #endregion

        #endregion




        


        
        #endregion

        #region SHOW AND HIDE LOADING FRAGMENT
        /// Показываю и закрыва экран загрузки в виде фрагмента экрана
        /// 

        Android.Support.V7.App.AlertDialog.Builder alert;
        Android.Support.V7.App.AlertDialog alertDialog;


        /// Показываю фрагмент загрузки
        /// 
        void ShowProgressDialogue() {

            alert = new Android.Support.V7.App.AlertDialog.Builder(activity);
            alert.SetView(Resource.Layout.progress);
            alert.SetCancelable(false);

            alertDialog = alert.Show();            
        }

        /// Закрываю фрагмент загрузки
        /// 

        void CloseProgressDialogue() {

            if (alert != null) {
                alertDialog.Dismiss();

                alertDialog = null;
                alert = null;
            }
        }
        #endregion



    }
}
