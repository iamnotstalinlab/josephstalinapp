package crc640a91f0301c8709bc;


public class MarketAdapter_MarketAdapterViewHolder
	extends androidx.recyclerview.widget.RecyclerView.ViewHolder
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("JosephStalinForTheProletarians.Adapter.MarketAdapter+MarketAdapterViewHolder, JosephStalinForTheProletarians", MarketAdapter_MarketAdapterViewHolder.class, __md_methods);
	}


	public MarketAdapter_MarketAdapterViewHolder (android.view.View p0)
	{
		super (p0);
		if (getClass () == MarketAdapter_MarketAdapterViewHolder.class)
			mono.android.TypeManager.Activate ("JosephStalinForTheProletarians.Adapter.MarketAdapter+MarketAdapterViewHolder, JosephStalinForTheProletarians", "Android.Views.View, Mono.Android", this, new java.lang.Object[] { p0 });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
