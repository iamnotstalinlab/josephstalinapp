﻿//Сервер который обрабатывает предложения
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using JosephStalinServer.DataModels;
using JosephStalinServer.EventListeners;
using Newtonsoft.Json;

namespace JosephStalinServer {
    class MainClass {

        // Запускаю программу
        //

        public static void Main(string[] args) {
            MainClass main = new MainClass();
            
            main.Create();
            main.LoadSession();

            Thread server = new Thread(new ThreadStart(main.Run));
            server.Start();

            //Обработка консольных команд
            string command = null;
            bool isRun =true;
            
            while (isRun) {
                Console.WriteLine("-----------------------------------------");
                Console.WriteLine("Команды:" +
                "\n s - статистика"+
                "\n q - выйти\n");

                command = Console.ReadLine();
                switch (command) {
                    case "s":
                        main.ShowStatistic(true, false);
                    break;

                    case "q":
                        Environment.Exit(0);
                    break;
                }
            }

            return;
        }


        //// Создаю сервер и определяю все вспомогательные объекты
        ///

        Session session;

        IPEndPoint ipPoint;
        static int port = 1212121212;

        TelegramServerBot tgServerBot;

        System.Timers.Timer tNotify;

        private void Create() {

            ////Обрабатываю безопасность для телеграм бота
            string tittle = "Введите токен для телеграм бота системы:";
            Console.WriteLine(tittle);

            ////Принимаю токен для телеграм бота
            string botKey = Console.ReadLine();
            tgServerBot = new TelegramServerBot(botKey);


            //Устанавливаю таймер на уведомления в телеграм

            DateTime nHour = DateTime.Now.AddHours(1)
                .AddMinutes(-DateTime.Now.Minute);

            double nTime = (nHour - DateTime.Now).TotalMilliseconds; //Вычисляю время до начала следущего часа

            tNotify = new System.Timers.Timer();
            tNotify.Interval = nTime;
            tNotify.Elapsed += Timer_Elapsed;
            tNotify.Start();

            //Получаем ip- адрес сервера
            string hostName = System.Net.Dns.GetHostName();
            System.Net.IPAddress ipAddress = System.Net.Dns.GetHostByName(hostName).AddressList[0];

            //Получаем адреса для запуска сокета
            ipPoint = new IPEndPoint(IPAddress.Parse(ipAddress.ToString()), port);
        }




        #region SERVER
        //// Запускаю сервер
        ///

        int thCount =0;
        const int MAX_THREADS = 10;
        private void Run() {

            //Создаём сокет
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            try {
                //Связываем сокет с точкой с которой будем принимать запросы
                socket.Bind(ipPoint);

                //Начинаем принимать заявки
                socket.Listen(80);
                Console.WriteLine($"Сервер запустился по адресу {ipPoint.ToString()}. Сокет принимает заявки.");

                while (true) {
                    //Создаём обрабочик из принятого сокета
                    //В цикле ничего страшного нет, поток блокируется 
                    //на функции socket.Accept() и ждёт подключения. 
                    //Только когда будет новое подключение, он продолжит выполняться.

                    Socket accept = socket.Accept();
                    while(thCount > MAX_THREADS) {}

                    lock (locker) {
                        ++thCount;
                    }
                    Thread thread = new Thread(new ParameterizedThreadStart(ExchangeData));
                    thread.Start(accept);
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            } finally {
                socket.Close();
            }
        }

        //// Обеспечиваю обмен данными в потоке
        ///

        //Блокировщик данных чтобы потоки не мешали друг-другу
        private static object locker = new object();

        private void ExchangeData(object gSocket) {
            TcpConnector guest = new TcpConnector((Socket)gSocket);
            GetOrSendOffers(ref guest);
            
        }

        //// Принимаю запрос и определяю от кого он
        ///

        private void GetOrSendOffers(ref TcpConnector guest) {
            try {
                
                string json = guest.GetData();
                Request tRequest = JsonConvert.DeserializeObject<Request>(json);

                switch (tRequest.Role) {
                    case "Помощник":
                        AcceptHelper(ref guest, ref tRequest);
                        break;
                    case "Плательщик":
                        AcceptPayer(ref guest, ref tRequest);
                        break;
                    case "Оператор":
                        AcceptOperator(ref guest, ref tRequest);
                        break;
                }

                if (guest.Connected) {
                    guest.Close();
                }

                lock (locker) {
                    SaveSession();
                }

    
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.Source);
                Console.WriteLine(ex.TargetSite);
                Console.WriteLine(ex.HResult);

                int line =GetLineNumber(ex);
                Console.WriteLine($"\n Ошибка в строке {line}");
            } finally {
                lock (locker) {
                    thCount--;
                }
            }
        }

        //Отлавливаю номер строки с ошибкой
        //

        public int GetLineNumber(Exception ex){
            var lineNumber = 0;
            const string lineSearch = ":line ";
            var index = ex.StackTrace.LastIndexOf(lineSearch);
            if (index != -1){
                var lineNumberText = ex.StackTrace.Substring(index + lineSearch.Length);
                if (int.TryParse(lineNumberText, out lineNumber)){}
            }
            return lineNumber;
        }


        //// Принимаю и обрабатываю запрос от плательщика
        ///

        private void AcceptPayer(ref TcpConnector tGuest, ref Request tRequest) {

            if (tRequest.Content == null) {

                //Если команды нет, значит только запустил приложение, отдаю текущую рыночную ставку
                Request aRequest = new Request("Сервер","Сервер", "MARKET RATE", mRate, null);
                
                //Упаковываю посылку в сообщение
                string message = JsonConvert.SerializeObject(aRequest);

                //Отправляю посылку через связного tGuest
                tGuest.SendData(message);
            } else {
                AddNewOffer(ref tRequest);
                tGuest.SendData("ВЫПОЛНИЛ");
            }
        }


        //Принимаем новое предложение
        private void AddNewOffer(ref Request tRequest) {
            FullOfferInfo nOffer = JsonConvert.DeserializeObject<FullOfferInfo>(tRequest.Content);

            if (!offers.Contains(nOffer)) {
                lock (locker) {
                    nOffer.Id = idOffer;
                    offers.Add(nOffer);

                    CalculateMarketRate(ref nOffer);
                    ++idOffer;

                    Console.WriteLine($"Предложений в списке {offers.Count}");
                }
            }
        }



        #region AcceptHelper
        //// Принимаю и обрабатываю запрос от помощника
        ///

        private void AcceptHelper(ref TcpConnector tGuest, ref Request tRequest) {
            
            //Запаковываем послылку помощнику
            string message = string.Empty;
            switch (tRequest.Command) {

                case "Войти":
                    Login(ref tGuest, ref tRequest);
                    break;

                case "Выполнить":
                    SendOffers(ref tGuest);
                    break;

                case "Завершить":
                    FinishOffer(ref tGuest, ref tRequest);
                    break;
            }
        }

        // Делаю проверку и провожу пользователя в систему
        //

        private void Login(ref TcpConnector inGuest, ref Request inRequest) {

            string phone = inRequest.From;
            string pin = inRequest.Content;

            User tUser = users.Find((obj) => obj.Phone == phone);
            Request rAnswer =new Request("Сервер","Сервер", null, 0, null);

            if (tUser ==null) {

                tUser =new User(phone, pin);
                tUser.Access =true;
                users.Add(tUser);
                rAnswer.Status = "УСПЕХ";

            } else {
                if((tUser.Pin == pin)
                    && tUser.Access) {
                    rAnswer.Status = "УСПЕХ";
                } else {
                    rAnswer.Status ="ОШИБКА";
                }
            }
            string message = JsonConvert.SerializeObject(rAnswer);
            inGuest.SendData(message); 
        }

        // Отправляю список свободных предложений
        //

        private void SendOffers(ref TcpConnector tGuest) {

            
            string content = JsonConvert.SerializeObject(offers, Formatting.Indented);
            Request aRequest = new Request("Сервер","Служащий", "LIST", mRate, content);

            //Упаковываю посылку в сообщение
            string message = JsonConvert.SerializeObject(aRequest);

            //Отправляю посылку через связного tGuest
            tGuest.SendData(message);
        }

        // Завершаю предложение и убираю его из списка свободных
        //

        private void FinishOffer(ref TcpConnector tGuest, ref Request tRequest) {
            FullOfferInfo offer = JsonConvert.DeserializeObject<FullOfferInfo>(tRequest.Content);

            //Удаляю из списка принятое приглашение.
            FullOfferInfo remove = offers.Find((obj) => obj.Id == offer.Id);
            if (remove != null) {
                lock (locker) {

                    ReCalculateMarketRate(ref remove);  //Пересчитываю среднерыночную ставку
                    CalculateLabourTime(ref offer); // Считаю среднерыночную ставку если предложение ещё свободно

                    Console.WriteLine($"{DateTime.Now.ToString()} Участник {tRequest.From} принял приглашение.");
                    offers.Remove(remove);  //Удаляю предложение из списка
                }
            }
        }

        #endregion

        //Если особая ситуация, то выполняю исключение/восстановление участника
        //

        private void AcceptOperator(ref TcpConnector tGuest, ref Request tRequest) {
            switch (tRequest.Command) {
                case "Допустить":
                    RemoveFromUserList(tRequest.Content);
                    break;
                case "Исключить":
                    CloseAccess(tRequest.Content);
                    break;
                case "Приглашение":
                    AddNewOffer(ref tRequest);
                    break;
            }
            tGuest.SendData("ВЫПОЛНИЛ");
        }

        #endregion


        List<User> users;

        //Закрываю пользователю доступ к системе
        //

        private void CloseAccess(string phone) {
            User tUser = users.Find((obj) => obj.Phone == phone);
            tUser.Access = false;
        }

        // Сбрасываю данные пользователя
        //

        private void RemoveFromUserList(string phone) {

            User tUser = users.Find((obj) => obj.Phone == phone);
            if(tUser != null) {
                users.Remove(tUser);
            }
        }




        #region SESSION

        ////Загружаю сеанс для работы
        ///

        //Общерыночная зарплата и рабочее время
        double marketMoney;
        double marketTimeHours;

        //Указанные в заявке время и оплата
        double offHours;
        double offWage;

        //Полные затраты времени и денег помощником
        double fullHours;
        double fullWage;

        //Рыночная ставка
        int mRate;

        //Общие данные
        int offAccept;

        //Список предложений
        List<FullOfferInfo> offers;

        //Индекс для того чтобы ориентироваться в списке
        int idOffer = 0;

        private void LoadSession() {

            //Создаю сеанс и загружаю предыдущие данные
            session = new Session();
            session = session.Load();
            
            //Заполняю переменные значениями
            offers = session.Offers;
            idOffer = session.IdOffer;

            offAccept = session.OffersAccept;
            mRate = session.MarketRate;

            marketMoney =session.MarketMoney;
            marketTimeHours = session.MarketTimeHours;

            fullWage = session.FullWage;
            fullHours = session.FullHours;

            offWage = session.OffersWage;
            offHours = session.OffersHours;

            users = session.Users;
        }

        //// Сохраняю текущий сеанс
        ////

        private void SaveSession() {

            session.Date = DateTime.Now.Date;
            session.Users = users;

            session.OffersHours = offHours;
            session.OffersWage = offWage;

            session.FullHours = fullHours;
            session.FullWage = fullWage;

            session.MarketRate = mRate;

            session.MarketMoney =marketMoney;
            session.MarketTimeHours =marketTimeHours;

            session.OffersAccept = offAccept;
            session.OffersCount = idOffer;

            session.IdOffer = idOffer;
            session.Offers = offers;

            session.Save();
        }

        // Обновляю данные по рынку
        //

        private void RefreshMarket() {
            session.Offers = offers;
            
            session.MarketTimeHours = marketTimeHours;
            session.MarketMoney =marketMoney;
            session.MarketRate =mRate;

            session.Save();
        }

        //// Показываю статистику
        ///

        private void ShowStatistic(bool inConsole, bool inTelegram) {
            int offCount = idOffer;

            string tittle = $"Статистика дня:\n";
            if (inConsole) {

                tittle += $"\nПриглашения:" +
                    $"\nЗарегистрировали: {offCount} (шт)" +
                    $"\nПриняли: {offAccept} (шт)" +
                    $"\nСвободные: {offers.Count} (шт)" +

                    $"\n\nОбщее:" +
                    $"\nЗаработок: {Math.Round(offWage, 1)} (руб)" +
                    $"\nРабочее время: {Math.Round(offHours, 1)} (час)" +
                    $"\nТрудозатраты: {Math.Round(fullHours, 1)} (час)" +

                    $"\n\nРыночная ставка: {mRate} (руб/час)";
                Console.WriteLine(tittle);
            }

            if (inTelegram) {

                tittle += $"\n<b>Приглашения:</b>" +
                    $"\nЗарегистрировали: <b>{offCount}</b> (шт)" +
                    $"\nПриняли: <b>{offAccept}</b> (шт)" +
                    $"\nСвободные: <b>{offers.Count}</b> (шт)" +

                    $"\n\n<b>Общее:</b>" +
                    $"\nЗаработок: <b>{Math.Round(offWage, 1)}</b> (руб)" +
                    $"\nРабочее время: <b>{Math.Round(offHours, 1)}</b> (час)" +
                    $"\nТрудозатраты: <b>{Math.Round(fullHours, 1)}</b> (час)" +

                    $"\n\nРыночная ставка: <b>{mRate}</b> (руб/час)";

                tgServerBot.SendSystemMessage(tittle);
            }


        }





        #endregion

        #region OFFERS

        // Считаю среднерыночную ставку при поступлении предложения
        //

        private void CalculateMarketRate(ref FullOfferInfo offer) {
            double oHours =GetOfferTimeHours(ref offer);

            marketTimeHours += oHours;
            marketMoney += offer.Wage;

            if(marketTimeHours > 0) {
                double tRate = marketMoney /marketTimeHours;
                tRate =Math.Floor((tRate /10)*10);
                mRate = Convert.ToInt32(tRate);
            }
        }

        // Пересчитываю среднерыночную ставку при удалении предложения
        //

        private void ReCalculateMarketRate(ref FullOfferInfo offer) {
            double oHours =GetOfferTimeHours(ref offer);

            marketTimeHours -= oHours;
            marketMoney -= offer.Wage;

            if(marketTimeHours > 0) {
                double tRate = marketMoney /marketTimeHours;
                tRate =Math.Floor((tRate /10)*10);
                mRate = Convert.ToInt32(tRate);
            } else {
                mRate =0;
            }
        }


        //Получаю время из предложения в часах
        //

        private double GetOfferTimeHours(ref FullOfferInfo offer) {

            //Собираю указанное в приглашении трудовое время
            DateTime toTime = DateTime.Parse(offer.ToTime);
            DateTime fromTime = DateTime.Parse(offer.FromTime);

            //Вычисляю время обозначенное в предложении
            return (toTime.Hour - fromTime.Hour) + ((toTime.Minute - fromTime.Minute) / 60);
        }

        //// Вычисляю рыночную ставку и все статистические показатели
        ///

        private void CalculateLabourTime(ref FullOfferInfo offer) {

            //Собираю указанное в приглашении трудовое время
            DateTime toTime = DateTime.Parse(offer.ToTime);
            DateTime fromTime = DateTime.Parse(offer.FromTime);

            //Вычисляю время обозначенное в предложении
            double oHours =GetOfferTimeHours(ref offer);

            //Вычисляю время которое затратит помощник
            double fHours = offer.FullTimeMins / 60;

            //Собираю данные для вычислений
            offHours += oHours; // Общее рабочее время
            fullHours += fHours; // Общие трудозатраты
            offWage += offer.Wage; //Общий заработок

            ++offAccept;
        }
        #endregion

        #region TIMERS

        //// Показываю статистику в телеграм чате по сигналу таймера
        //// 

        bool isFirst =true;
        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e) {

            if (isFirst) {
                tNotify.Interval = 1000 * 60 * 60; //Устанавливаю новый интервал через час от этого
                isFirst = false;
            }

            lock (locker) {
                ClearOldOffers();

                RefreshMarket(); // обновляю данные по рынку
                LoadSession(); // чтобы обновить данные если изменилась дата (это знает LoadSession)
            }


            if(DateTime.Now.Hour %4 == 0) {
                Console.WriteLine($"{DateTime.Now.Hour}:{DateTime.Now.Minute}");
                ShowStatistic(false, true);
            }
        }


        // Удаляю старые предложения
        //

        object offersLock;
        
        DateTime today;
        DateTime nowTime;

        DateTime dayRequest;
        DateTime timeRequest;
        FullOfferInfo off;

        private void ClearOldOffers() {

            today = DateTime.Today;
            nowTime = DateTime.Now;

            
            for (int i = 0; i < offers.Count; ++i) {
                off = offers[i];                
                dayRequest = DateTime.Parse(off.Date, CultureInfo.InvariantCulture);
                timeRequest = DateTime.Parse(off.FromTime, CultureInfo.InvariantCulture);
    
                //Удаляю вчерашние приглашения
                if (dayRequest < today) {
                    
                    ReCalculateMarketRate(ref off);
                    offers.Remove(off);
                } else {


                    //Удаляю сегодняшние прошедшие
                    if(dayRequest < today) {
                        ReCalculateMarketRate(ref off);
                        offers.Remove(off);
                    }

                    if (dayRequest == today) {
                        if (timeRequest.Hour < nowTime.Hour) {   
                            ReCalculateMarketRate(ref off);
                            offers.Remove(off);
                        } 
                        if(timeRequest.Hour == nowTime.Hour) {
                            if(timeRequest.Minute < nowTime.Minute) {
                                ReCalculateMarketRate(ref off);
                                offers.Remove(off);
                            }
                        }

                    }
                }
            }

            
            


        }

        #endregion

    }
}
