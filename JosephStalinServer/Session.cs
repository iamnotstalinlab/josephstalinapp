﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Security.AccessControl;
using System.Text;
using JosephStalinServer.DataModels;
using Newtonsoft.Json;

namespace JosephStalinServer {

    //Текущий рабочий сеанс
    public class Session {

        //Переменная с путём к файлу
        string path;
        public Session() {
            //Узнаю текущую папку с сервером чтобы туда положить файл сохранения
            path = Directory.GetCurrentDirectory();
            path += "/SessionData.txt";

            //Если файла нет, то заполняю по данным с 22 по 25 строку
            Date = DateTime.Now.Date;
            MarketRate = 200;

            Offers = new List<FullOfferInfo>();
            Users =new List<User>();
        }

        //Список исключённых участников
        public List<User> Users {get;set;}

        //Все данные которые следует иметь для работы сеанса
        public DateTime Date { get; set; }

        //Общерыночная зарплата и рабочее время
        public double MarketMoney{get; set;}
        public double MarketTimeHours {get; set;}

        //Указанные в заявке время и оплата
        public double OffersHours { get; set; }
        public double OffersWage { get; set; }

        //Полные затраты времени и денег помощником
        public double FullHours { get; set; }
        public double FullWage { get; set; }

        //Рыночная ставка
        public int MarketRate { get; set; }

        //Общие данные
        public int OffersAccept { get; set; }
        public int OffersCount { get; set; }

        public int IdOffer { get; set; }

        public List<FullOfferInfo> Offers { get; set; }

        //Сохраняю сериализированный объект сеанса в папку сервера
        public void Save() {
            string json = JsonConvert.SerializeObject(this);
            File.WriteAllText(path, json);
        }


        //Загружаю сохранение сеанса. 
        public Session Load() {
            if (File.Exists(path)) {
                string json = File.ReadAllText(path);

                //Загружаю последнюю сессию в переменную, для проверки даты
                Session lSession = JsonConvert.DeserializeObject<Session>(json);

                /// Меня интересует сегодняшнее сохранение.
                if(this.Date == lSession.Date) {
                    return lSession;
                } else {
                    MarketRate = lSession.MarketRate;   //Загружаю действующую рыночную ставку
                    MarketMoney = lSession.MarketMoney;     //Загружаю действующие рыночные деньги
                    MarketTimeHours = lSession.MarketTimeHours; //Загружаю действующее рыночное рабочее время

                    Offers = lSession.Offers;   //Беру действующие предложния
                    Users = lSession.Users;   //Беру список пользователей
                } 
            }
            return this;
        }




    }
}
