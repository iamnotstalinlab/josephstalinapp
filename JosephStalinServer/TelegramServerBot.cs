﻿using System;
using System.Threading.Tasks;
using Telegram.Bot;

namespace JosephStalinServer {
    public class TelegramServerBot {

        //Устанавливаю уникальный идентификатор общего чата
        long chatId = -1001362707960;

        public TelegramServerBot() {
        }


        /// Создаю бота
        /// 

        ITelegramBotClient bot;
        public TelegramServerBot(string key) {
            bot = new TelegramBotClient(key);
        }

        //Отправляю сообщение в чат
        //

        public async Task SendSystemMessage(string title) {
            await bot.SendTextMessageAsync(chatId: chatId
                , text: title
                , parseMode: Telegram.Bot.Types.Enums.ParseMode.Html);
        }
    }
}
