﻿//Объект запроса
//

namespace JosephStalinServer.DataModels {

public class Request {
        public string From { get; set; }    //От кого пришёл запрос Плательщик/Помощник
        public string Role { get; set; }    //Роль пользователя

        public string Command { get; set; } //Особенная команда на выполнение
        public int MarketRate { get; set; } //Среднерыночная ставка
        public string Status { get; set; }  //Статус запроса

        public string Content { get; set; }

        public Request(string from, string role, string command, int marketRate, string content) {
            From = from;
            Role =role;
            Command = command;
            Content = content;
            MarketRate = marketRate;
        }
    }
}
