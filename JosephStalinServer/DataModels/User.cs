﻿using System;
namespace JosephStalinServer.DataModels {
    public class User {

        public string Phone { get; set;}
        public string Pin { get; set;}
        public bool Access{ get; set;}

        public User(string phone, string pin) {
            Phone =phone;
            Pin = pin;
        }
    }
}
