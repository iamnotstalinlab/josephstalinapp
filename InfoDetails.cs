﻿using System;
namespace JosephStalin.DataModels {
    public class FullInfo : BaseInfo {

        public string PickUpAddress { get; set; }
        public string PayMentMethod { get; set; }

        public bool SpecificDocuments { get; set; }
        public bool SpecificView { get; set; }
        public bool Documents { get; set; }

        public string Comment { get; set; }

        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }

        public string PromoterPhone { get; set; }
        public string PromoterName { get; set; }

        public string Status { get; set; }

        public DateTime Timestamp { get; set; }

        FullInfo() { }

        public FullInfo(BaseInfo info) {
            Date = info.Date;
            FromTime = info.FromTime;
            ToTime = info.ToTime;
            Wage = info.Wage;

            FullTimeMins = info.FullTimeMins;
            PickUpLat = info.PickUpLat;
            PickUpLng = info.PickUpLng;
            InvitationId = info.InvitationId;
        }
    }


    public class BaseInfo {
        public string Date { get; set; }

        public string FromTime { get; set; }
        public string ToTime { get; set; }

        public int Wage { get; set; }
        public double FullTimeMins { get; set; }

        public double PickUpLat { get; set; }
        public double PickUpLng { get; set; }


        public string InvitationId { get; set; }

        
    }
}
